import React from 'react';
import { ActivityIndicator, View } from 'react-native';

const LoadingScreen = (props) => {
  return (
    <View style={{ backgroundColor: '#fff', width: '100%', height: '100%' }}>
      <ActivityIndicator color='#009740' />
    </View>
  )
}

export default LoadingScreen;
