import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import * as WebBrowser from 'expo-web-browser';
import {
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
} from 'react-native';
import Switch from 'react-native-switch-pro';
import { ListItem } from 'react-native-elements';
import { ToastView } from '../components';
import { UserAPI } from '../api/user';
import { setCurrentUser } from '../actions';
import {HeaderButtons, Item} from "react-navigation-header-buttons";
import {AppHeaderIcon} from "../updateAppMain/components/HeaderButton";
import {TimeScreen} from "../updateAppMain/screens/TeamsScreen";

const phone = '+7 099 123 45 67';

const SettingsScreen = ({ navigation, currentUser, setCurrentUser }) => {
  const phone = currentUser !== null ? currentUser.user : '';
  // console.log('SETTINGS', currentUser);
  // const [_authLoad, _setAuthLoad] = useState(false);
  // const [_auth, _setAuth] = useState(false);

  // useEffect(() => {
  //   getUser();
  // },[navigation]);
  //
  // const getUser = async () => {
  //   const data = await AsyncStorage.getItem('user');
  //   console.log('>>>>> USER DATA', data);
  //   if(data !== null) {
  //     console.log('OKKKAY');
  //     setCurrentUser(JSON.parse(data));
  //     _setAuthLoad(true);
  //     _setAuth(true);
  //   } else {
  //     _setAuthLoad(true);
  //     _setAuth(false);
  //     navigation.navigate('RegAuth');
  //     ToastView('Пожалуйста, авторизуйтесь');
  //   }
  // }

  const logout = () => {
    setCurrentUser(null);
    UserAPI.logout()
    .then(res => {
      console.log('LOGOUT', res);
      // navigation.navigate('RegAuth');
      AsyncStorage.clear()
      .then(resStorage => {
        console.log('STORAGE LOGOUT', resStorage);
        ToastView('Вы успешно вышли из аккаунта');
      })
    });
  }

  const handleOpenBrowserAsync = async (link) => {
    let result = await WebBrowser.openBrowserAsync(link);
    console.log('handleOpenBrowserAsync --- ', result);
  };

  return (
    <ScrollView style={styles.container}>
      <>
        {/*<Text style={[styles.paddingLeft, styles.label]}>Настройка аккаунта</Text>*/}
        <ListItem
          title="Номер телефона"
          rightElement={( <Text style={styles.phone}>{ phone }</Text> )}
          titleStyle={{
            color: '#000000',
            fontFamily: 'Montserrat500',
            fontSize: 17,
            lineHeight: 24,
            opacity: 0.7,
          }}
          containerStyle={{
            height: 62,
          }}
        />


        {/*<Text style={[styles.paddingLeft, styles.label]}>Уведомления</Text>
        <ListItem
          title="Push-уведомления"
          rightElement={( <Switch onSyncPress={value => console.log('SWITCH', value)} width={37} height={22} backgroundActive='#0C0054' /> )}
          titleStyle={{
            color: '#000000',
            fontFamily: 'Montserrat500',
            fontSize: 17,
            lineHeight: 24,
            opacity: 0.7,
          }}
          containerStyle={{
            height: 62,
          }}
        />*/}


        {/*<Text style={[styles.paddingLeft, styles.label]}>Связь</Text>
        <ListItem
          chevron
          title="Связаться с нами"
          titleStyle={{
            color: '#000000',
            fontFamily: 'Montserrat500',
            fontSize: 17,
            lineHeight: 24,
            opacity: 0.7,
          }}
          containerStyle={{
            height: 62,
          }}
        />
        <ListItem
          chevron
          title="Помощь и поддержка"
          titleStyle={{
            color: '#000000',
            fontFamily: 'Montserrat500',
            fontSize: 17,
            lineHeight: 24,
            opacity: 0.7,
          }}
          containerStyle={{
            height: 62,
          }}
        />*/}


        <Text style={[styles.paddingLeft, styles.label]}>Юридические сведения</Text>
        <ListItem
          chevron
          title="Конфиденциальность"
          titleStyle={{
            color: '#000000',
            fontFamily: 'Montserrat500',
            fontSize: 17,
            lineHeight: 24,
            opacity: 0.7,
          }}
          containerStyle={{
            height: 62,
          }}
          onPress={() => handleOpenBrowserAsync('https://futbol-0.flycricket.io/privacy.html')}
        />
        {/*<ListItem
          chevron
          title="Соглашение пользователя"
          titleStyle={{
            color: '#000000',
            fontFamily: 'Montserrat500',
            fontSize: 17,
            lineHeight: 24,
            opacity: 0.7,
          }}
          containerStyle={{
            height: 62,
          }}
        />*/}

        <TouchableOpacity activeOpacity={0.6} style={[styles.logoutBtn, styles.paddingLeft]} onPress={() => logout()}><Text style={styles.logoutTxt}>Выйти из аккаунта</Text></TouchableOpacity>
      </>

      {/*{ currentUser !== null
        ?

        :
          <ActivityIndicator color='#009740' style={{ paddingTop: 17 }} />
      }*/}

    </ScrollView>
  )
};

SettingsScreen.navigationOptions = ({ navigation }) => ({
  headerTitle: 'Настройки',
  headerLeft: () => (
      <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
        <Item
            title='Toggle Drawer'
            iconName='ios-menu'
            onPress={() => navigation.toggleDrawer()}
        />
      </HeaderButtons>
  )
});

const mapStateToProps = state => {
  return {
    currentUser: state.currentUser.currentUser
  }
};

const mapDispatchToProps = dispatch => {
  return {
    setCurrentUser: (user) => {
      dispatch(setCurrentUser(user))
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingsScreen);

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F7F7F7',
    // paddingTop: 27,
  },
  label: {
    paddingTop: 29,
    paddingBottom: 12,
    color: '#000000',
    fontSize: 14,
    fontFamily: 'Montserrat500',
    opacity: 0.54,
  },
  phone: {
    color: '#0C0054',
    fontFamily: 'Montserrat500',
    fontSize: 17,
    lineHeight: 24,
  },
  paddingLeft: {
    paddingLeft: 16,
  },
  logoutBtn: {
    paddingVertical: 19,
    paddingBottom: 50,
  },
  logoutTxt: {
    color: '#0C0054',
    fontFamily: 'Montserrat600',
    fontSize: 14,
  },
});
