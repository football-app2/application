import React, { useState } from 'react';
import { connect } from 'react-redux';
import Constants from 'expo-constants';
import {
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  Dimensions,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Button,
  Text,
  View,
  AsyncStorage,
  Platform,
} from 'react-native';

import { RegisterForm, LoginForm, ToastView, SpinnerView } from '../../components';
const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);
//
import { UserAPI } from '../../api/user';
import { setCurrentUser } from '../../actions';

const RegAuthScreen = ({ navigation, scene, previous, setCurrentUser }) => {
  const [_isLoading, _setLoading] = useState(false);
  const [_showLogin, _setShowLogin] = useState(true);
  const [_confirmFocus, _setConfirmFocus] = useState(false);
  console.log('NAV - ', navigation);
  console.log('SCENE - ', scene);
  console.log('PREVIOUS - ', previous);

  const handleSubmitRegForm = (values, resetForm) => {
    _setLoading(true);
    try {
      UserAPI.register(values)
        .then(response => {
          console.log('REGISTER - ',response);
          if(response.access_token !== undefined) {
            // navigation.navigate('Main', { screen: 'Teams' });
            AsyncStorage.setItem('user', JSON.stringify(response)).then((res) => {
              setCurrentUser(response);
              // navigation.navigate('Table');
              ToastView('Вы успешно зарегистрированы!');
              _setLoading(false);
            });
          } else {
            ToastView('Пожалуйста, проверьте правильность введённых данных');
            _setLoading(false);
          }
        });
    } catch (e) {
      console.log('UserAPI.register | ', e);
      ToastView('ошибка, попробуйте позже');
      resetForm();
      _setLoading(false);
    }
  };

  const handleSubmitLoginForm = async (values, resetForm) => {
    _setLoading(true);
    try {
      auth(values);
    } catch (e) {
      resetForm();
      ToastView('Ошибка, попробуйте позже');
      _setLoading(false);
    }
  };

  const auth = (values) => {
    UserAPI.login(values)
      .then(response => {
        console.log('LOGIN - ',response);
        if(response.access_token !== undefined) {
          AsyncStorage.setItem('user', JSON.stringify(response)).then(res => {
            setCurrentUser(response);
            // navigation.navigate('Table');
            ToastView('Добро пожаловать!');
            _setLoading(false);
          });
        } else {
          ToastView('Пожалуйста, проверьте правильность введённых данных');
          _setLoading(false);
        }
      });
  }

  const handleSkipAuth = () => {
    const values = {
      phone: '89101490463',
      password: '123456'
    }

    _setLoading(true);
    try {
      auth(values);
    } catch (e) {
      ToastView('Ошибка, попробуйте позже');
      _setLoading(false);
    }
  }

  return (

    <View style={styles.container}>
      {/*<View style={styles.statusBar} />*/}

      <ImageBackground source={require('../../assets/splash.png')} style={styles.ImageBackground}>
        <ScrollView style={[styles.scrollView, _confirmFocus && styles.paddingOnFocus]}>

        {_isLoading && <SpinnerView />}

          <View style={styles.main}>

            <View style={styles.selectsContainer}>
              <TouchableOpacity
                style={styles.selectBtn}
                activeOpacity={0.7}
                onPress={() => _setShowLogin(prevState => !prevState)}
              >
                <Text style={_showLogin ? styles.selectBtnTextActive : styles.selectBtnText}>Авторизация</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.selectBtn}
                activeOpacity={0.7}
                onPress={() => _setShowLogin(prevState => !prevState)}
              >
                <Text style={_showLogin ? styles.selectBtnText : styles.selectBtnTextActive}>Регистрация</Text>
              </TouchableOpacity>
            </View>

            {/*<KeyboardAvoidingView behavior={"padding"} enabled>*/}
            <View style={styles.textCenter}>
              { _showLogin
                ?
                  <LoginForm
                    onHandleSubmitLogForm={handleSubmitLoginForm}
                  />
                :
                  <RegisterForm
                    onHandleSubmitRegForm={handleSubmitRegForm}
                    setConfirmFocus={_setConfirmFocus}
                  />
              }
            </View>
            {/*</KeyboardAvoidingView>*/}

            {/*<TouchableOpacity
              onPress={handleSkipAuth}
              activeOpacity={0.8}
              style={{ paddingTop: 20 }}
            >
              <Text style={{ color: '#fff', textAlign: 'center', fontWeight: '500', fontSize: 16 }}>пропустить</Text>
            </TouchableOpacity>*/}

            {/*<Button
              onPress={() => navigation.navigate('Teams')}
              title="Teams"
            />*/}
          </View>
        </ScrollView>
      </ImageBackground>

    </View>

  )
};

const mapDispatchToProps = dispatch => {
  return {
    setCurrentUser: (user) => {
      dispatch(setCurrentUser(user))
    }
  }
};

export default connect(null, mapDispatchToProps)(RegAuthScreen);

const styles = StyleSheet.create({
  statusBar: {
    height: Constants.statusBarHeight,
  },
  container: {
    // backgroundColor: '#009740',
    // flex: 1,
  },
  ImageBackground: {
    width: '100%',
    height: '105%',
  },
  paddingOnFocus: {
    paddingTop: width / 7,
  },
  scrollView: {
    flex: 1,
    paddingTop: width / 3,
  },
  main: {
    flex: 1,
    zIndex: 1,
  },
  logoBlock: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  formBlock: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textCenter: {
    flex: 1,
    alignItems: 'center'
  },
  load: {
    width: 42,
    height: 40,
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
  registerText: {
    fontSize: 10,
    color: '#fff',
    flex: 1
  },
  registerTextWrap: {
    marginTop: 12,
    alignItems: 'center',
    flex: 0.1,
  },
  selectsContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-start',
    flexDirection: 'row',
    marginBottom: 44,
  },
  selectBtn: {
    backgroundColor: 'transparent',
    marginHorizontal: 8,
  },
  selectBtnText: {
    color: '#fff',
    fontSize: 20,
    fontFamily: 'Montserrat500',
    opacity: 0.6,
  },
  selectBtnTextActive: {
    color: '#fff',
    fontFamily: 'Montserrat500',
    fontSize: 20,
    paddingBottom: 4,
    borderBottomWidth: 1.5,
    borderBottomColor: '#fff',
  },
});
