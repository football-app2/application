import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Constants from 'expo-constants';
import {
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  Dimensions,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Button,
  Text,
  View,
  AsyncStorage,
  Platform,
} from 'react-native';
import * as WebBrowser from 'expo-web-browser';

import { RegisterForm, LoginForm, ToastView, SpinnerView } from '../../components';
const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);
//
import { UserAPI } from '../../api/user';
import { setCurrentUser } from '../../actions';

const PaymentScreen = ({ navigation, scene, previous, currentUser, setCurrentUser }) => {
  const [_isLoading, _setLoading] = useState(false);
  const [_showLogin, _setShowLogin] = useState(true);
  const [_confirmFocus, _setConfirmFocus] = useState(false);
  const [_resultBrowser, _setResultBrowser] = useState(null);

  // console.log('NAV - ', navigation);
  // console.log('SCENE - ', scene);
  // console.log('PREVIOUS - ', previous);

  console.log('Payment user - ', currentUser);
  const phone = currentUser !== null && currentUser.user ? currentUser.user : '';
  const access_token = currentUser !== null && currentUser.access_token ? currentUser.access_token : '';

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getUser();
    });

    return unsubscribe;
  }, []);

  useEffect(() => {
    getUser();
  }, [_resultBrowser])

  const getUser = () => {
    _setLoading(true);

    console.log('GET USER PAYMENT - ');

    try {
      UserAPI.getUser()
        .then(response => {
          console.log('RESPONSE', response);
          if(access_token !== '' && response.status !== 'error' && parseInt(response.status) !== 0) {
            let userObj = {
              user: phone,
              access_token,
              status: response.status,
            }

            AsyncStorage.setItem('user', JSON.stringify(userObj)).then((res) => {
              console.log('');
              setCurrentUser(response);
              navigation.navigate('Main');
              // ToastView('Добро пожаловать!');
              _setLoading(false);
            });
          } else {
            _setLoading(false);
          }
        })
    } catch (e) {
      console.log('UserAPI.getUser | ', e);
      ToastView('Ошибка при обновлении данных');
      _setLoading(false);
    }
  }

  const logout = () => {
    setCurrentUser(null);
    UserAPI.logout()
    .then(res => {
      console.log('LOGOUT', res);
      navigation.navigate('RegAuth');
      AsyncStorage.clear()
      .then(resStorage => {
        console.log('STORAGE LOGOUT', resStorage);
        ToastView('Вы успешно вышли из аккаунта');
      })
    });
  }

  const handleOpenBrowserAsync = async (link) => {
    let result = await WebBrowser.openBrowserAsync(link);
    console.log('handleOpenBrowserAsync --- ', result);

    _setResultBrowser(result);
  };

  return (

    <View style={styles.container}>
      <ImageBackground source={require('../../assets/splash-blur.jpg')} style={styles.ImageBackground}>
        <ScrollView style={styles.scrollView}>

        {_isLoading && <SpinnerView />}

            <View style={styles.textCenter}>
              <Text
                style={{
                  fontFamily: 'Montserrat700',
                  color: '#fff',
                  textAlign: 'center',
                  fontSize: 23,
                  lineHeight: 29,
                  marginBottom: 15,
                }}
              >
                Для продолжения необходимо оплатить аккаунт
              </Text>

              <Text
                style={{
                  fontFamily: 'Montserrat700',
                  color: '#fff',
                  textAlign: 'center',
                  fontSize: 25,
                  marginBottom: 15,
                }}
              >
                349₽
              </Text>

              <TouchableOpacity
                onPress={() => handleOpenBrowserAsync(`http://football.weedoo.ru/pay/?phone=${phone}`)}
                activeOpacity={0.8}
                style={{
                  backgroundColor: '#fff',
                  borderRadius: 94,
                  paddingVertical: 15,
                  paddingHorizontal: 35,
                }}
              >
                <Text
                  style={{
                    color: '#000',
                    textAlign: 'center',
                    fontFamily: 'Montserrat700',
                    fontSize: 18
                  }}
                >
                  Оплатить
                </Text>
              </TouchableOpacity>

              <Text
                style={{
                  color: '#fff',
                  textAlign: 'center',
                  fontFamily: 'Montserrat400',
                  fontSize: 14,
                  marginTop: 5,
                }}
              >
                оплата проводится единоразово, аккаунт будет действовать бесконечно
              </Text>
            </View>

            <TouchableOpacity
              onPress={() => getUser()}
              style={{
                flex: 1,
                paddingTop: 30,
              }}
            >
              <Text
                style={{
                  textAlign: 'center',
                  color: '#fff',
                  textDecorationLine: 'underline',
                  fontSize: 19,
                }}
              >
                Проверить статус оплаты
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => logout()}
              style={{
                flex: 1,
                paddingTop: 30,
              }}
            >
              <Text
                style={{
                  textAlign: 'center',
                  color: '#fff',
                }}
              >
                выйти из аккаунта
              </Text>
            </TouchableOpacity>

        </ScrollView>
      </ImageBackground>
    </View>

  )
};

const mapStateToProps = state => {
  return {
    currentUser: state.currentUser.currentUser
  }
};

const mapDispatchToProps = dispatch => {
  return {
    setCurrentUser: (user) => {
      dispatch(setCurrentUser(user))
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentScreen);

const styles = StyleSheet.create({
  statusBar: {
    height: Constants.statusBarHeight,
  },
  container: {
    // backgroundColor: '#009740',
    // flex: 1,
  },
  ImageBackground: {
    width: '100%',
    height: '105%',
  },
  paddingOnFocus: {
    paddingTop: width / 7,
  },
  textCenter: {
    flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 15,
  },
  scrollView: {
    flex: 1,
    paddingTop: width / 2.1,
  },
  main: {
    flex: 1,
    zIndex: 1,
  },
  logoBlock: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  formBlock: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  load: {
    width: 42,
    height: 40,
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
  registerText: {
    fontSize: 10,
    color: '#fff',
    flex: 1
  },
  registerTextWrap: {
    marginTop: 12,
    alignItems: 'center',
    flex: 0.1,
  },
  selectsContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-start',
    flexDirection: 'row',
    marginBottom: 44,
  },
  selectBtn: {
    backgroundColor: 'transparent',
    marginHorizontal: 8,
  },
  selectBtnText: {
    color: '#fff',
    fontSize: 20,
    fontFamily: 'Montserrat500',
    opacity: 0.6,
  },
  selectBtnTextActive: {
    color: '#fff',
    fontFamily: 'Montserrat500',
    fontSize: 20,
    paddingBottom: 4,
    borderBottomWidth: 1.5,
    borderBottomColor: '#fff',
  },
});
