import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Constants from 'expo-constants';
import {
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  ActivityIndicator,
  AsyncStorage,
  Dimensions,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
} from 'react-native';
import {
  Table,
  ToastView,
} from '../components';
import { ListItem } from 'react-native-elements';
import { TeamAPI } from '../api/team';
import { IMGDOMAIN } from '../api/utils/config';
import { setCurrentUser } from '../actions';
import {HeaderButtons, Item} from "react-navigation-header-buttons";
import {AppHeaderIcon} from "../updateAppMain/components/HeaderButton";
import {SearchAPI} from "../api/search";

const TeamScreen = (props) => {
  const { currentUser, setCurrentUser } = props;
  const [_data, _setData] = useState([]);
  const [_trainerAvatar, _setTrainerAvatar] = useState('');

  const getTeammembersByTeamId = () => {
    if(true) {
      TeamAPI.getTeammembersByTeamId(1)
      .then(res => {
        if(res) {
         res= {
           ...res,
           trainer: {
             name: res.trainer ? res.trainer.name : '',
             avatar: res.trainer ? res.trainer.avatar : '',
             id: res.trainer ? res.trainer.id : ''
           }
         }
          _setData(res);
          _setTrainerAvatar(`${IMGDOMAIN}${res.trainer.avatar}`);
          console.log('TEAM', `${IMGDOMAIN}${res.trainer.avatar}`);
        }
      })
      .catch(err => console.error(err))
    }
  }

  useEffect(() => {
    getTeammembersByTeamId();
  },[]);

  return (
    <ScrollView style={{ backgroundColor: '#fff' }}>
      { _data.length !== 0 && _trainerAvatar.length !== 0
        ?
          <>
            <ListItem
              leftAvatar={{ rounded: true, source: { uri: _trainerAvatar } }}
              title={_data.trainer.name}
              titleStyle={{
                color: '#2B2A30',
                fontFamily: 'Montserrat600',
                fontSize: 16,
                lineHeight: 20,
                letterSpacing: -0.02,
              }}
              subtitle='Тренер'
              subtitleStyle={{
                color: '#000',
                opacity: 0.7,
                fontFamily: 'Montserrat600',
                fontSize: 12,
                lineHeight: 15,
                letterSpacing: -0.02,
              }}
              containerStyle={{
                backgroundColor: '#fff',
                height: 88,
              }}
              />
            <Table tableData={_data.players} type='teammembers' />
          </>
        :
          <ActivityIndicator color='#009740' style={{ paddingTop: 17 }} />
      }
    </ScrollView>
  )
};

TeamScreen.navigationOptions = ({ navigation }) => ({
  headerTitle: 'Команды',
  headerRight: () => (
      <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
        <Item
            title='Search'
            iconName='ios-search'
            onPress={() => navigation.navigate('Search')}
        />
      </HeaderButtons>
  ),
  headerLeft: () => (
      <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
        <Item
            title='Toggle Drawer'
            iconName='ios-menu'
            onPress={() => navigation.toggleDrawer()}
        />
      </HeaderButtons>
  )
});

const mapStateToProps = state => {
  return {
    currentUser: state.currentUser.currentUser
  }
};

const mapDispatchToProps = dispatch => {
  return {
    setCurrentUser: (user) => {
      dispatch(setCurrentUser(user))
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(TeamScreen);
