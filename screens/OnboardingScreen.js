import React from 'react'
import {
  Image,
  ImageBackground,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
} from 'react-native';

const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);

const OnboardingScreen = ({ navigation }) => {
  return (
    <ImageBackground source={require('../assets/onboarding.jpg')} style={{ width, height }}>
      <TouchableOpacity
        onPress={() => navigation.navigate('Teams')}
        style={{
          width: '100%',
          height: 53,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Text style={{
          fontFamily: 'Montserrat500',
          fontSize: 20,
          color: '#fff',
        }}>
          Начать
        </Text>
      </TouchableOpacity>
    </ImageBackground>
  )
}

export default OnboardingScreen
