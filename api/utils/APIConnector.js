import { AsyncStorage } from 'react-native';
import axios from 'axios';
import config from '../utils/config';

const SERVER_URL = config.serverURL;
const TOKEN = config.token;

const APIMethods = {
  PUT: 'PUT',
  GET: 'GET',
  POST: 'POST',
  DELETE: 'DELETE',
};

// TOKEN.then(r => {
//   console.log('TTTOOKKEEN', r.access_token);
// })

const request = async (
  route,
  method,
  params,
  token = true,
) => {
  let user = await AsyncStorage.getItem('user');
  const query = new URLSearchParams(params).toString();
  const url = `${SERVER_URL}${route}?${query}`;
  const headers = token ? { 'Authorization': `Bearer ${JSON.parse(user).access_token}` } : {};

  let body = {
    url,
    headers,
    method: APIMethods[method],
  }

  if(APIMethods[method] !== 'GET'){
    body['data'] = params;
  }

    return axios(body)
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log('====================================');
      console.log('axios.error', error);
      return { status: 'error' };
    });
  // })
}

class API {
  url = '';

  constructor(url = '') {
    this.url = url;
  }

  GET = (internal = '', params, token) => request(this.url + internal, APIMethods.GET, params, token);

  POST = (internal = '', params, token) => request(this.url + internal, APIMethods.POST, params, token);

  PUT = (internal = '', params, token) => request(this.url + internal, APIMethods.PUT, params, token);

  DELETE = (internal = '', params, token) => request(this.url + internal, APIMethods.DELETE, params, token);
}

export default API;
