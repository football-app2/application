import API from '../utils/APIConnector';
import config from '../utils/config';

const Request = new API();
const TOKEN = config.token;

export class UserAPI {

  static register = async (props) => await Request.POST('/register', { ...props }, false);

  static login = async (props) => await Request.POST('/login', { ...props }, false);

  static logout = async () => await Request.GET('/logout');

  static getProfile = async (props) => await Request.GET('/profile/get-profile', { ...props });

  static getUser = async (props) => await Request.GET('/user', { ...props });

  static updateUser = async (props) => await Request.PUT('/user/update', { ...props });

};
