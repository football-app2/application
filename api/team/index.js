import API from '../utils/APIConnector';
import config from '../utils/config';

const Request = new API();
const TOKEN = config.token;

export class TeamAPI {

  static getTeamsMatches = async (page) => await Request.GET(`/team/matches`, {page}, false);
  static getTeammembersByTeamId = async (id) => await Request.GET(`/team/members?id=${id}`, {}, false);
  static getTeamsTable = async (props) => await Request.GET('/team/rating', {...props}, false); // params: to=2100&from=1900&champType=1
  static getTeamsTableByCat = async (id, props) => await Request.GET(`/category/${id}/rating`, {...props}, false); // params: to=2100&from=1900&champType=1

};
