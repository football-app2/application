import API from '../utils/APIConnector';
import config from '../utils/config';

const Request = new API();
const TOKEN = config.token;

export class SearchAPI {

  //to=2100&from=1900&champType=1
  static search = async (props) => await Request.GET(`/search`, {...props}, false);

};
