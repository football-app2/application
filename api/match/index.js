import API from '../utils/APIConnector';
import config from '../utils/config';

const Request = new API();
const TOKEN = config.token;

export class MatchAPI {

  //to=2100&from=1900&champType=1
  static getRounds = async (props) => await Request.GET('/match/round', {...props}, false);
  static getTours = async (props) => await Request.GET('/match/tour', {...props}, false);
  static getRoundsByCat = async (id, props) => await Request.GET(`/category/${id}/round`, {...props}, false);
  static getToursByCat = async (id, props) => await Request.GET(`/category/${id}/tour`, {...props}, false);

};
