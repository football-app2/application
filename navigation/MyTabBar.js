import React from 'react';
import { connect } from 'react-redux';
import { View, TouchableOpacity, SafeAreaView, Text, StyleSheet, Image, Platform } from 'react-native';
import { ButtonGroup, Icon } from 'react-native-elements';

function MyTabBar({ state, descriptors, navigation, position, currentUser }) {
  const buttons = ['Чемпионат города']; //'Команды',
  const updateIndex = () => {
    if(state.index === 0){
      navigation.navigate('Table');
    } else {
      navigation.navigate('Teams');
    }
  }

  console.log('currentUser', currentUser);

  return (
    <>
    <SafeAreaView style={{flex: 0, backgroundColor: '#009740'}}>
    <View style={styles.container}>
      <View style={styles.innerContainer}>
      <ButtonGroup
          onPress={updateIndex}
          selectedIndex={state.index}
          buttons={buttons}
          containerBorderRadius={4}
          containerStyle={{
            // width: width - width / 4,
            // width: width - 95,
            flex: 1,
            height: 34,
            backgroundColor: '#009740',
            // borderWidth: 0
          }}
          textStyle={{
            fontSize: 11,
            lineHeight: 20,
            color: '#fff',
            fontFamily: 'Montserrat400',
            letterSpacing: -0.2,
          }}
          selectedTextStyle={{
            color: '#009740',
          }}
          buttonStyle={{
            // width: '100%',
            // display: 'flex',
            flex: 1,
          }}
          selectedButtonStyle={{
            backgroundColor: '#fff',
          }}
        />
      <View style={styles.rightContainer}>
          <TouchableOpacity activeOpacity={0.6} onPress={() => navigation.navigate('Search')}>
            <Image source={require('../assets/imgs/search.png')} style={styles.rightIcon} />
          </TouchableOpacity>

          { currentUser.user !== '89101490462' && (
            <TouchableOpacity activeOpacity={0.6} onPress={() => navigation.navigate('Settings')}>
              <Image source={require('../assets/imgs/settings.png')} style={styles.rightIcon} />
            </TouchableOpacity>
          )}
        </View>
      </View>
    </View>
    </SafeAreaView>
    </>
  );
}

const mapStateToProps = state => {
  return {
    currentUser: state.currentUser.currentUser
  }
};

export default connect(mapStateToProps, null)(MyTabBar);

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#009740',
    justifyContent: 'flex-end',
    paddingVertical: 13,
    paddingTop: Platform.OS === 'ios' ? 13 : 30,
    paddingHorizontal: 13,
    //height: 116,
  },
  innerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  rightContainer: {
    flexDirection: 'row',
  },
  rightIcon: {
    marginHorizontal: 9,
  },
});
