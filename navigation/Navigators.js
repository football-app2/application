import React from 'react';
// import {
//   createBottomTabNavigator,
//   // createStackNavigator,
//   createSwitchNavigator,
// } from 'react-navigation';
import { createStackNavigator } from '@react-navigation/stack';

import {
  CompanyScreen,
  ArticleScreen,
  ArticlesScreen,
  ArticlesCategoriesScreen,
  CreateArticleScreen,
  AuthLoadingScreen,
  BookmarksScreen,
  CompaniesScreen,
  ConfirmationScreen,
  ContactsScreen,
  CreateCompanyScreen,
  EditProfileScreen,
  HomeScreen,
  LoginScreen,
  ProfileScreen,
  RegisterScreen,
  SupportScreen,
  SupportMailScreen,
  PoliticsScreen,
  RepairsScreen,
  ShopsScreen,
  ServicesScreen
} from '../screens';
// import { TabBarIcon } from '../components';

const RegisterStack = createStackNavigator();

export default

// const RegisterStack = createStackNavigator({
//   Login: {
//     screen: LoginScreen,
//   },
//   Register: {
//     screen: RegisterScreen,
//   },
//   Confirmation: {
//     screen: ConfirmationScreen,
//   },
//   Politics: {
//     screen: PoliticsScreen,
//   }
// }, {
//   headerMode: 'none'
// }
// );
//
// const HomeStack = createStackNavigator({
//   Home: {
//     screen: HomeScreen,
//   },
//   Repairs: {
//     screen: RepairsScreen,
//   },
//   Services: {
//     screen: ServicesScreen,
//   },
//   Shops: {
//     screen: ShopsScreen,
//   }
// }, {
//   headerMode: 'none',
//   initialRouteKey: 'Home'
// }
// );
//
// const AuthStack = createStackNavigator({
//   SignIn: RegisterStack
// }, {
//   headerMode: 'none',
// });
//
// const rootNavigator = createSwitchNavigator({
//   AuthLoading: AuthLoadingScreen,
//   App: bottomTabNavigator,
//   Auth: AuthStack
// }, {
//   initialRouteName: 'AuthLoading'
// })

// export default rootNavigator;
