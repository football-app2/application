import React from 'react';
import { connect } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import {
  RegAuthScreen,
  PaymentScreen,
  TeamsScreen,
  TableScreen,
  TeamScreen,
  SettingsScreen,
  SearchScreen,
  OnboardingScreen,
  LoadingScreen,
} from '../screens';
import { Header, HeaderSearch, HeaderBig } from '../components';
import { getCurrentUser } from '../actions';
import MyTabBar from './MyTabBar';
import {UpdateAppNavigation} from "../updateAppMain/navigation/UpdateAppNavigation";

const Stack = createStackNavigator();
const Tab = createMaterialTopTabNavigator();

const config = {
  animation: 'spring',
  config: {
    duration: 0,
  },
};

function Main() {
  return (
    <Tab.Navigator tabBar={props => <MyTabBar {...props} />}>
      {/*<Tab.Screen name="Teams" component={TeamsScreen} />*/}
      <Tab.Screen name="Table" component={TableScreen} />
    </Tab.Navigator>
  );
}

function AppNavigator(props) {
  const auth = props.auth;
  const load = props.load;
  const currentUser = props.currentUser;
  const userStatus = currentUser !== null && currentUser.status !== undefined && currentUser.status !== null && currentUser.status !== 'error' ? currentUser.status : 0;

  if(!load){
      return (
          <NavigationContainer>
              <Stack.Navigator
                  screenOptions={{
                      gestureEnabled: false,
                  }}
                  headerMode='screen'>
                  <Stack.Screen
                      name="Loading"
                      component={LoadingScreen}
                      options={{ headerShown: false, headerLeft: null }}/>
              </Stack.Navigator>
          </NavigationContainer>
      )
  }

  if(currentUser !== null){
      if(parseInt(userStatus) === 1){
          return (
              <NavigationContainer>
                  <Stack.Navigator
                      screenOptions={{
                          gestureEnabled: false,
                      }}
                      headerMode='screen'>
                      <Stack.Screen
                          name="Payment"
                          component={PaymentScreen}
                          options={{
                              headerShown: false,
                              headerLeft: null,
                          }}
                      />
                  </Stack.Navigator>
              </NavigationContainer>
          )
      }
      return (
          <UpdateAppNavigation />
      )
  }

  return (
    <NavigationContainer>
        <Stack.Navigator
            screenOptions={{
                gestureEnabled: false,
            }}
            headerMode='screen'>
            <Stack.Screen
                name="RegAuth"
                component={RegAuthScreen}
                options={{
                    headerShown: false,
                    headerLeft: null,
                }}
            />

            <Stack.Screen
                name="Payment"
                component={PaymentScreen}
                options={{
                    headerShown: false,
                    headerLeft: null,
                }}
            />

        </Stack.Navigator>
    </NavigationContainer>
  );
}

const mapStateToProps = state => {
  return {
    currentUser: state.currentUser.currentUser
  }
};

export default connect(mapStateToProps, null)(AppNavigator);

{/*{!load ? (*/}
{/*  <Stack.Screen name="Loading" component={LoadingScreen} options={{ headerShown: false, headerLeft: null }} />*/}
{/*) : currentUser !== null ? (*/}
{/*  <>*/}
{/* <Stack.Screen
              name="Teams"
              component={TeamsScreen}
              options={{
              headerShown: false,
              gestureEnabled: false,
              header: (props) => <Header {...props} />,
              headerLeft: null,
              transitionSpec: {
              open: config,
              close: config,
              },
              ...TransitionPresets.SlideFromRightIOS,
              }}
              />
              <Stack.Screen
              name="Table"
              component={TableScreen}
              options={{
              header: (props) => <Header {...props} />,
              transitionSpec: {
              open: config,
              close: config,
              },
              ...TransitionPresets.SlideFromRightIOS,
              }}
              />*/}

{/*{ parseInt(userStatus) === 1 ? (*/}
{/*  <Stack.Screen*/}
{/*    name="Payment"*/}
{/*    component={PaymentScreen}*/}
{/*    options={{*/}
{/*      headerShown: false,*/}
{/*      headerLeft: null,*/}
{/*    }}*/}
{/*  />*/}
{/*) : <></>}*/}

{/*  <Stack.Screen name="Main" component={Main}*/}
{/*    options={{*/}
{/*      // header: (props) => <Header {...props} />,*/}
{/*      headerShown: false,*/}
{/*      headerLeft: null,*/}
{/*      transitionSpec: {*/}
{/*        open: config,*/}
{/*        close: config,*/}
{/*      },*/}
{/*      ...TransitionPresets.SlideFromRightIOS, }} />*/}

{/*    <Stack.Screen*/}
{/*      name="Team"*/}
{/*      component={TeamScreen}*/}
{/*      options={{*/}
{/*        header: (props) => <HeaderBig type='full' {...props} />,*/}
{/*      transitionSpec: {*/}
{/*        open: config,*/}
{/*        close: config,*/}
{/*      },*/}
{/*      ...TransitionPresets.SlideFromRightIOS, }} />*/}

{/*  <Stack.Screen*/}
{/*    name="Settings"*/}
{/*    component={SettingsScreen}*/}
{/*    options={{*/}
{/*      header: (props) => <HeaderBig type='simple' {...props} />,*/}
{/*    title: 'Настройки',*/}
{/*    /*transitionSpec: {*/}
{/*    open: config,*/}
{/*    close: config,*/}
{/*  },*/}
{/*...TransitionPresets.SlideFromRightIOS,*/}
{/*}} />*/}

{/*  <Stack.Screen*/}
{/*    name="Search"*/}
{/*    component={SearchScreen}*/}
{/*    options={{*/}
{/*      headerShown: true,*/}
{/*      headerTitle: false,*/}
{/*      header: (props) => <HeaderSearch {...props} />,*/}
{/*    transitionSpec: {*/}
{/*      open: config,*/}
{/*      close: config,*/}
{/*    },*/}
{/*    ...TransitionPresets.SlideFromRightIOS, }} />*/}

{/*<Stack.Screen
                name="RegAuth"
                component={RegAuthScreen}
                options={{
                headerShown: false,
                headerLeft: null,
              }}
              />*/}

{/*<Stack.Screen*/}
{/*  name="Onboarding"*/}
{/*  component={OnboardingScreen}*/}
{/*  options={{*/}
{/*  headerShown: false,*/}
{/*  headerLeft: null,*/}
{/*}}*/}
{/*/>*/}
{/*</>*/}
{/*) : (*/}
{/*  <>*/}
{/*    <Stack.Screen*/}
{/*      name="RegAuth"*/}
{/*      component={RegAuthScreen}*/}
{/*      options={{*/}
{/*        headerShown: false,*/}
{/*        headerLeft: null,*/}
{/*      }}*/}
{/*    />*/}

{/*    <Stack.Screen*/}
{/*      name="Payment"*/}
{/*      component={PaymentScreen}*/}
{/*      options={{*/}
{/*        headerShown: false,*/}
{/*        headerLeft: null,*/}
{/*      }}*/}
{/*    />*/}
{/*  </>*/}
{/*)}*/}