import React, { useState, useEffect } from 'react';
import Constants from 'expo-constants';
import {
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  Dimensions,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
} from 'react-native';

import { ButtonGroup, Icon } from 'react-native-elements';

const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);

export const Header = (props) => {
  const scene = props.scene;
  const navigation = props.navigation;



  const [_selectedIndex, _setSelectedIndex] = useState('Teams');

 /* useEffect(() => {
    if(scene.route.name === 'Teams') {
      _setSelectedIndex(0);
    } else {
      _setSelectedIndex(1);
    }
  }, [scene.route])*/

  const updateIndex = (selectedIndex) => {
    if(_selectedIndex === 'Teams'){
      _setSelectedIndex('Table');
      navigation.navigate('Table');
    } else {
      _setSelectedIndex('Teams');
      navigation.navigate('Teams');
    }
   /* if(scene.route.name === 'Teams') {
      navigation.navigate('Table');
    } else {
      navigation.navigate('Teams');
    }*/
    /*if(selectedIndex === 0) {
      navigation.navigate('Teams', { selectedIndex });
      _setSelectedIndex(0);
    } else if(selectedIndex === 1) {
      navigation.navigate('Table', { selectedIndex });
      _setSelectedIndex(1);
    }*/
  }

  console.log(scene.route.name);

  // const component1 = () => <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', borderWidth: 1, borderColor: '#fff' }}><Text>Команды</Text></View>
  // const component2 = () => <View style={{ flex: 2, alignItems: 'center', justifyContent: 'center', borderWidth: 1, borderColor: '#fff' }}><Text>Турнирная таблица</Text></View>

  // const buttons = [{ element: component1 }, { element: component2 }];
  const buttons = ['Команды', 'Турнирная таблица'];

  return (
    <View style={styles.container}>
      <View style={styles.innerContainer}>
        <ButtonGroup
          onPress={updateIndex}
          selectedIndex={_selectedIndex === 'Table' ? 1 : 0}
          buttons={buttons}
          containerBorderRadius={4}
          containerStyle={{
            // width: width - width / 4,
            // width: width - 95,
            // flex: 1,
            height: 34,
            backgroundColor: '#009740',
            // borderWidth: 0
          }}
          textStyle={{
            fontSize: 12,
            lineHeight: 20,
            color: '#fff',
            fontFamily: 'Montserrat400',
            letterSpacing: -0.2,
          }}
          selectedTextStyle={{
            color: '#009740',
          }}
          buttonStyle={{
            // width: '100%',
            // display: 'flex',
            flex: 1,
          }}
          selectedButtonStyle={{
            backgroundColor: '#fff',
          }}
        />

        <View style={styles.rightContainer}>
          <TouchableOpacity activeOpacity={0.6} onPress={() => navigation.navigate('Search')}>
            <Image source={require('../../assets/imgs/search.png')} style={styles.rightIcon} />
          </TouchableOpacity>
          <TouchableOpacity activeOpacity={0.6} onPress={() => navigation.navigate('Settings')}>
            <Image source={require('../../assets/imgs/settings.png')} style={styles.rightIcon} />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#009740',
    justifyContent: 'flex-end',
    paddingVertical: 13,
    height: 116,
  },
  innerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  rightContainer: {
    flexDirection: 'row',
  },
  rightIcon: {
    marginHorizontal: 9,
  },
});
