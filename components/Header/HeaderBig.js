import React, { useState, useEffect } from 'react';
import Constants from 'expo-constants';
import {
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  Dimensions,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
} from 'react-native';

import { Icon } from 'react-native-elements';
import { IMGDOMAIN } from '../../api/utils/config';

const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);

export const HeaderBig = (props) => {
  const scene = props.scene;
  const previous = props.previous;
  const navigation = props.navigation;
  const buttons = ['Команды', 'Турнирная таблица'];
  const { options } = scene.descriptor;
  let title =
    options.headerTitle !== undefined
      ? options.headerTitle
      : options.title !== undefined
      ? options.title
      : scene.route.name;
  title = title.length > 14 ? `${title.slice(0,14)}..` : title;

  let teamTitle = scene.route.params !== undefined ? scene.route.params.team.name : '';
  teamTitle = teamTitle.length > 14 ? `${teamTitle.slice(0,14)}..` : teamTitle;

  const teamAvatar = scene.route.params !== undefined && scene.route.params.team.avatar !== undefined ? `${IMGDOMAIN}/${scene.route.params.team.avatar}` : 'https://cdni.rt.com/russian/images/2018.07/article/5b3b711e18356119048b4615.JPG';
  return (
    <View style={styles.container}>
      <TouchableOpacity activeOpacity={0.6} onPress={() => navigation.goBack()} style={[styles.backBtn, props.type === 'simple' && { marginBottom: 19 }]}>
        <Icon
          type='font-awesome'
          name='angle-left'
          color='#FFFFFF'
        />
      <Text style={styles.backText}>Back</Text>
      </TouchableOpacity>

      <View style={styles.innerContainer}>
        <View style={styles.leftContainer}>
          <Text style={styles.title}>{ props.type === 'simple' ? title : teamTitle}</Text>
        </View>

        { props.type !== 'simple' && (
          <View style={styles.rightContainer}>
            <Image
              resizeMethod="resize"
              source={{ uri: teamAvatar }}
              style={styles.rightIcon}
            />
              {/*<Image
        style={{width: 50, height: 50}}
        source={{uri: 'https://facebook.github.io/react/img/logo_og.png'}}
      />*/}
          </View>
        )}
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#009740',
    justifyContent: 'flex-end',
    paddingVertical: 13,
    height: 140,
  },
  title: {
    fontFamily: 'SFProDisplay700',
    letterSpacing: 0.6,
    fontSize: 34,
    lineHeight: 41,
    color: '#fff',
  },
  backBtn: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginLeft: 8.5,
    marginBottom: 6.5,
  },
  backText: {
    color: '#fff',
    fontSize: 17,
    lineHeight: 22,
    marginLeft: 5,
    paddingTop: 1,
  },
  innerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
  },
  rightContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    width: 56,
    height: 56,
    borderRadius: 28,
  },
  rightIcon: {
    width: 56,
    height: 56,
    borderRadius: 28,
    marginHorizontal: 9,
  },
});
