import React, { useState, useEffect } from 'react';
import Constants from 'expo-constants';
import {
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  Dimensions,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  Platform,
} from 'react-native';

import { SearchBar, Icon } from 'react-native-elements';
import { SafeAreaView } from 'react-navigation';


export const HeaderSearch = (props) => {
  const navigation = props.navigation;
  // const scene = props.scene;
  // const previous = props.previous;
  // const { options } = scene.descriptor;
// console.log('SEARCH - ', navigation);
  const [search, setSearch] = useState('');

  useEffect(() => {
    console.log('Poisk - ', search);
  }, [search])

  const updateSearch = (value) => {
    console.log('SEARCH', value);
    setSearch(value);
  }

  const clearSearch = () => {
    props.scene.descriptor.options.handleChange();
  }

  return (
    <SafeAreaView style={{backgroundColor: '#009740'}}>
    <View style={styles.container}>
      <View style={styles.innerContainer}>
      <TouchableOpacity activeOpacity={0.6} onPress={() => navigation.goBack()} style={[styles.backBtn, props.type === 'simple' && { marginBottom: 19 }]}>
        <Icon
          type='font-awesome'
          name='angle-left'
          color='#fff'
        />
      <Text style={styles.backText}>Back</Text>
      </TouchableOpacity>
      </View>
    </View>
    </SafeAreaView>
  )
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#009740',
    justifyContent: 'flex-end',
    //height: 116,
  },
  innerContainer: {
   // alignItems: 'center',
    flexDirection: 'column'
  },
  backBtn: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginLeft: 8.5,
    marginBottom: 6.5,
  },
  backText: {
    color: '#fff',
    fontSize: 17,
    lineHeight: 22,
    marginLeft: 5,
    paddingTop: 1,
  },
});
