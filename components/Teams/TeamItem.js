import React, { useState } from 'react';
import Constants from 'expo-constants';
import { ListItem } from 'react-native-elements';
import {
  Image,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
} from 'react-native';
import { IMGDOMAIN } from '../../api/utils/config';

const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);

export const TeamItem = ({
  navigation,
  team,
  matches,
}) => {
  return (
    <View>
      <ListItem
        // chevron
        leftAvatar={{ rounded: true, source: { uri: `${IMGDOMAIN}/${team.avatar}` } }}
        title={team.name}
        titleStyle={{
          color: '#009740',
          fontFamily: 'Montserrat600',
          fontSize: 16,
          lineHeight: 20,
          letterSpacing: -0.02,
        }}
        subtitle={team.city}
        subtitleStyle={{
          color: '#000',
          opacity: 0.7,
          fontFamily: 'Montserrat600',
          fontSize: 12,
          lineHeight: 15,
          letterSpacing: -0.02,
        }}
        containerStyle={{
          backgroundColor: '#F6F6F6',
          height: 72,
        }}
        // onPress={() => navigation.navigate('Team', { team })}
      />

      {
        matches.length !== 0 && matches.map((item, key) => (
          <TeamItemMatch
            key={key}
            from={item.teams[0].name}
            to={item.teams[1].name}
            date={item.date}
            teams={item.teams}
          />
        ))
      }

    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#009740',
  },
});


export const TeamItemMatch = ({ from, to, date, teams }) => {
  return (
    <View style={stylesMatch.container}>
      <View style={stylesMatch.teamsContainer}>
        <View style={stylesMatch.teamImgsContainer}>
          <Image source={teams[0].avatar !== undefined ? { uri: `${IMGDOMAIN}/${teams[0].avatar}` } : require('../../assets/imgs/teamImg.png')} style={stylesMatch.teamImg} />
          <View style={stylesMatch.line} />
          <Image source={teams[1].avatar !== undefined ? { uri: `${IMGDOMAIN}/${teams[1].avatar}` } : require('../../assets/imgs/teamImg.png')} style={stylesMatch.teamImg} />
        </View>

        <View style={stylesMatch.teamNamesContainer}>
          <Text style={stylesMatch.teamTop}>{ from }</Text>
          <Text style={stylesMatch.teamBottom}>{ to }</Text>
        </View>
      </View>

      <View style={stylesMatch.dateContainer}>
        <Text style={stylesMatch.dateTxt}>{ date }</Text>
      </View>
    </View>
  )
};

const stylesMatch = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#EFF1F0',
    height: 72,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  teamImgsContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 16,
  },
  teamImg: {
    width: 20,
    height: 20,
  },
  line: {
    width: 1,
    height: 12,
    backgroundColor: '#009740',
    marginVertical: 2,
  },
  teamsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  teamNamesContainer: {
    marginLeft: 12,
  },
  teamTop: {
    marginBottom: 8,
    letterSpacing: -0.2,
    fontFamily: 'Montserrat500',
    fontSize: 14,
  },
  teamBottom: {
    marginTop: 8,
    letterSpacing: -0.2,
    fontFamily: 'Montserrat400',
    fontSize: 14,
  },
  dateContainer: {
    // paddingTop: 26,
    // paddingBottom: 26,
    // paddingLeft: 18,
    // paddingRight: 18,
    alignItems: 'center',
    justifyContent: 'center',
    width: width / 4,
    height: 72,
    borderLeftWidth: 1,
    borderLeftColor: '#EFF1F0',
  },
  dateTxt: {
    color: '#0C0054',
    fontSize: 16,
    lineHeight: 20,
    fontFamily: 'Montserrat500',
    letterSpacing: -0.2,
  },
});
