import React, { useState } from 'react';
import {
  Image,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  FlatList,
  Text,
  View,
} from 'react-native';
import { TeamItem } from './TeamItem';
const {height} = Dimensions.get('window');

export const TeamItems = ({ teams, navigation, getTeams, updateStatus, status, handleMore }) => {
  return (
      <FlatList
        data={teams}
        renderItem={({ item }) => <TeamItem team={item.team} matches={item.matches} navigation={navigation} />}
        keyExtractor={(item, index) => item.team.id}
        onEndReachedThreshold={0.01}
        onEndReached={handleMore}
        //nestedScrollEnabled
        onMomentumScrollBegin={() => updateStatus(false)}
      />
  )
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#009740',
  },
});
