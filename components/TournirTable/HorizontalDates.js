import React, { useState, useEffect } from 'react';
import {
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Text,
  ScrollView,
  View,
} from 'react-native';

const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);

export const HorizontalDates = ({ categories, selectedCatID, setSelectedCatID, togglePressCategory }) => {
  console.log('SELECTED CAT ID', selectedCatID);

  if(categories && categories.length !== 0) {
    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={styles.datesContainer} alwaysBounceHorizontal={true} showsHorizontalScrollIndicator={false} horizontal={true}>
          {
            categories.map((category, index) => (
              <TouchableOpacity
                style={styles.date}
                activeOpacity={0.8}
                onPress={() => togglePressCategory(category.id)}
              >
                <Text style={[styles.dateTxt, selectedCatID === category.id && styles.dateTxtActive]}>{ category.name }</Text>
              </TouchableOpacity>
            ))
          }
        </ScrollView>
      </View>
    )
  } else {
    return <></>;
  }
};

const styles = StyleSheet.create({
  container: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderTopColor: '#EFF1F0',
    borderBottomColor: '#EFF1F0',
  },
  datesContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    flexWrap: 'nowrap',
    paddingVertical: 19,
  },
  date: {
    // width: width / 4 - 11,
    flex: 1,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 19,
  },
  dateTxt: {
    fontFamily: 'Montserrat500',
    fontSize: 15,
    lineHeight: 20,
    color: '#000000',
    opacity: 0.87,
    letterSpacing: -0.2,
  },
  dateTxtActive: {
    color: '#0C0054',
    fontFamily: 'Montserrat700',
    borderBottomWidth: 3,
    borderBottomColor: '#0C0054',
    letterSpacing: -0.2,
  },
});
