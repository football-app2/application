import React, { useState } from 'react';
import {
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  ActivityIndicator,
  Dimensions,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  FlatList
} from 'react-native';
// import { Table, TableWrapper, Row, Rows, Cell, Col } from 'react-native-table-component';
import TableCommandsHead from './TableCommands/TableCommandsHead';
import TableCommandsRow from './TableCommands/TableCommandsRow';
import TableRoundsHead from './TableRounds/TableRoundsHead';
import TableRoundsRow from './TableRounds/TableRoundsRow';
import TableCalendarHead from './TableCalendar/TableCalendarHead';
import TableCalendarRow from './TableCalendar/TableCalendarRow';
import TableTeammembersHead from './TableTeammembers/TableTeammembersHead';
import TableTeammembersRow from './TableTeammembers/TableTeammembersRow';

export const Table = ({ handleMore, navigation, tableHead, tableData, type, goToTeam }) => {
  const [_selectedBtn, _setSelectedBtn] = useState(0);
  const [_selectedDate, _setSelectedDate] = useState(0);


  return (
    <View style={styles.container}>
      <View>

        { type === 'commands' && (
          <>
            <TableCommandsHead tableHead={tableHead} />
            <FlatList

        data={tableData}
        renderItem={({ item, index }) => <TableCommandsRow
        key={index}
        num={index+1}
        team={item.team}
        z={item.z}
        p={item.p}
        o={item.o}
        goToTeam={goToTeam}
      />}
        nestedScrollEnabled={true}
        keyExtractor={(item, index) => index}
        onEndReachedThreshold={0.1}
        onEndReached={handleMore}
        //onMomentumScrollBegin={() => updateStatus(false)}
      />
            {/*
              tableData.map((item, index) => (
                <TableCommandsRow
                  key={index}
                  num={index+1}
                  team={item.team}
                  z={item.z}
                  p={item.p}
                  o={item.o}
                  goToTeam={goToTeam}
                />
              ))
              */}
          </>
        )}

        { type === 'rounds' && (
          <>
          <FlatList
            data={tableData}
            renderItem={({ item, index }) => <React.Fragment key={index}>
            <TableRoundsHead round={item.round} />

            {
              item.matches.map((item, key) => (
                <TableRoundsRow
                  key={key}
                  teams={item.teams}
                  date={item.date}
                  score={item.score}
                  goToTeam={goToTeam}
                />
              ))
            }
          </React.Fragment>}
            nestedScrollEnabled={true}
            keyExtractor={(item, index) => index}
            onEndReachedThreshold={0.1}
            onEndReached={handleMore}
            //onMomentumScrollBegin={() => updateStatus(false)}
          />
            {/*
              tableData.map((rootItem, index) => (
                <React.Fragment key={index}>
                  <TableRoundsHead round={rootItem.round} />

                  {
                    rootItem.matches.map((item, key) => (
                      <TableRoundsRow
                        key={key}
                        teams={item.teams}
                        date={item.date}
                        score={item.score}
                        goToTeam={goToTeam}
                      />
                    ))
                  }
                </React.Fragment>
              ))
                */}
          </>
        )}

        { type === 'calendar' && (
          <>
            {/*<TableCalendarHead tournament={tableHead} />

            {
              tableData.map((item, index) => (
                <TableCalendarRow
                  key={index}
                  teams={item.teams}
                  date={item.date}
                  goToTeam={goToTeam}
                />
              ))
            }*/}
<FlatList
            data={tableData}
            renderItem={({ item, index }) => <React.Fragment key={index}>
            <TableCalendarHead tournament={item.tour} />

            {
              item.matches.map((item, key) => (
                <TableCalendarRow
                  key={index}
                  teams={item.teams}
                  date={item.date}
                  stadium={item.stadium !== null ? item.stadium.name : '-'}
                  goToTeam={goToTeam}
                />
              ))
            }
          </React.Fragment>}
            nestedScrollEnabled={true}
            keyExtractor={(item, index) => index}
            onEndReachedThreshold={0.1}
            onEndReached={handleMore}
            //onMomentumScrollBegin={() => updateStatus(false)}
          />
            {
             /* tableData.map((rootItem, index) => (
                <React.Fragment key={index}>
                  <TableCalendarHead tournament={rootItem.tour} />

                  {
                    rootItem.matches.map((item, key) => (
                      <TableCalendarRow
                        key={index}
                        teams={item.teams}
                        date={item.date}
                        stadium={item.stadium !== null ? item.stadium.name : '-'}
                        goToTeam={goToTeam}
                      />
                    ))
                  }
                </React.Fragment>
              ))*/
            }
          </>
        )}

        { type === 'teammembers' && (
          <>
            <TableTeammembersHead />

            {
              tableData.map((item, index) => (
                <TableTeammembersRow
                  key={index}
                  index={index}
                  player={item}
                />
              ))
            }
          </>
        )}

      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1,
    paddingBottom: 7,
  },
});
