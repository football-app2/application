import React from 'react';
import {
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  Dimensions,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
} from 'react-native';

const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);

const TableCommandsHead = ({ tableHead }) => {
  return (
    <View style={styles.head}>
      <View style={[styles.cellCenter, { flex: 1 }]}>
        <Text style={[styles.teamCellText, styles.lightText]}>{ tableHead[0] }</Text>
      </View>

      <TouchableOpacity activeOpacity={0.8} style={[styles.teamCell, { flex: 4.5 }]}>
        <Text style={[styles.teamCellText, styles.lightText]}>{ tableHead[1] }</Text>
      </TouchableOpacity>

      <View style={[styles.cellCenter, styles.cellSmall, { flex: 0.9 }]}>
        <Text style={[styles.teamCellText, styles.mediumCell, styles.greenCell]}>{ tableHead[2] }</Text>
      </View>

      <View style={[styles.cellCenter, styles.cellSmall, { flex: 0.9 }]}>
        <Text style={[styles.teamCellText, styles.mediumCell, styles.redCell]}>{ tableHead[3] }</Text>
      </View>

      <View style={[styles.cellCenter, styles.cellSmall, { flex: 0.9 }]}>
        <Text style={[styles.teamCellText, styles.mediumCell, styles.yellowCell]}>{ tableHead[4] }</Text>
      </View>
    </View>
  )
};

export default TableCommandsHead;

const styles = StyleSheet.create({
  head: {
    backgroundColor: '#F6F6F6',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 36,
  },
  headText: {
    color: '#000000',
    opacity: 0.87,
    textAlign: 'center',
  },
  headLeft: {
    textAlign: 'left',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 5,
    height: 36,
  },
  text: {
    color: '#000',
  },
  cellCenter: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cellSmall: {
    width: width / 8,
    flex: 1,
  },
  teamCell: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: width / 2.8,
  },
  teamCellImg: {
    width: 20,
    height: 20,
    marginRight: 12,
  },
  teamCellText: {
    fontFamily: 'Montserrat400',
    fontSize: 16,
    lineHeight: 20,
    color: '#000000',
    opacity: 0.87,
  },
  mediumText: {
    fontFamily: 'Montserrat500',
    fontSize: 16,
    lineHeight: 20,
  },
  lightText: {
    fontFamily: 'Montserrat300',
  },
  greenCell: {
    color: '#009740',
  },
  redCell: {
    color: '#D00000',
  },
  yellowCell: {
    color: '#EEBA02',
  },
});
