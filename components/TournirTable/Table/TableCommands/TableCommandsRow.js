import React from 'react';
import {
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  Dimensions,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
} from 'react-native';

import { IMGDOMAIN } from '../../../../api/utils/config';

const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);

const TableCommandsRow = (props) => {
  const {
    num,
    team,
    z,
    p,
    o,
    goToTeam,
  } = props;
  return (
    <View style={styles.row}>
      <TouchableOpacity activeOpacity={1} style={[styles.teamCell, { flex: 5.2 }]} onPress={() => goToTeam(team)}>
        <View style={[styles.cellCenter, { flex: 1 }]}>
          <Text style={styles.teamCellText}>{ num }</Text>
        </View>
        <View style={{ flex: 1 }}>
          <Image source={{ uri: `${IMGDOMAIN}/${team.avatar}` }} style={styles.teamCellImg} />
        </View>
        <Text style={[styles.teamCellText, styles.teamName, { flex: 5.3 }]} numberOfLines={3}>{ team.name }</Text>
        <View style={[styles.cellCenter, styles.cellSmall, { flex: 1 }]}>
          <Text style={[styles.teamCellText, styles.mediumCell, styles.greenCell]} numberOfLines={1}>{ z }</Text>
        </View>

        <View style={[styles.cellCenter, styles.cellSmall, { flex: 1 }]}>
          <Text style={[styles.teamCellText, styles.mediumCell, styles.redCell]} numberOfLines={1}>{ p }</Text>
        </View>

        <View style={[styles.cellCenter, styles.cellSmall, { flex: 1 }]}>
          <Text style={[styles.teamCellText, styles.mediumCell, styles.yellowCell]} numberOfLines={1}>{ o }</Text>
        </View>
      </TouchableOpacity>
    </View>
  )
};

export default TableCommandsRow;

const styles = StyleSheet.create({
  head: {
    backgroundColor: '#F6F6F6',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 36,
  },
  headText: {
    color: '#000000',
    opacity: 0.87,
    textAlign: 'center',
  },
  headLeft: {
    textAlign: 'left',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 7,
    height: 'auto',
  },
  text: {
    color: '#000',
  },
  teamName: {
    lineHeight: 24,
  },
  cellCenter: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cellSmall: {
    width: width / 8,
    flex: 1,
  },
  teamCell: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: width / 2.8,
  },
  teamCellImg: {
    width: 20,
    height: 20,
    // marginRight: 12,
  },
  teamCellText: {
    fontFamily: 'Montserrat400',
    fontSize: 16,
    lineHeight: 20,
    color: '#000000',
    opacity: 0.87,
  },
  mediumText: {
    fontFamily: 'Montserrat500',
    fontSize: 16,
    lineHeight: 20,
  },
  lightText: {
    fontFamily: 'Montserrat300',
  },
  greenCell: {
    color: '#009740',
  },
  redCell: {
    color: '#D00000',
  },
  yellowCell: {
    color: '#EEBA02',
  },
});
