import React from 'react';
import {
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  Dimensions,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
} from 'react-native';

const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);

const TableCalendarRow = (props) => {
  const {
    teams,
    date,
    stadium,
    goToTeam,
  } = props;

  return (
    <View style={styles.row}>
      <View style={[styles.teamCell, { flex: 3 }]}>
        <TouchableOpacity activeOpacity={1}><Text style={styles.cellText} numberOfLines={1}>{ teams[0].name.length > 7 ? `${teams[0].name.slice(0,7)}..` : teams[0].name }</Text></TouchableOpacity>
        <Text style={styles.verticalLine}>|</Text>
        <TouchableOpacity activeOpacity={1}><Text style={styles.cellText} numberOfLines={1}>{ teams[1].name.length > 7 ? `${teams[1].name.slice(0,7)}..` : teams[1].name }</Text></TouchableOpacity>
      </View>

      <View style={[styles.cellCenter, { flex: 1.2, marginRight: 6 }]}>
        <Text style={styles.cellText} numberOfLines={1}>{ stadium }</Text>
      </View>

      <View style={[styles.cellCenter, { flex: 1, marginRight: 20 }]}>
        <Text style={styles.cellText}>{ date }</Text>
      </View>
    </View>
  )
};

export default TableCalendarRow;

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 5,
    paddingLeft: 16,
    height: 36,
  },
  text: {
    color: '#000',
  },
  cellCenter: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cellSmall: {
    width: width / 8,
    flex: 1,
  },
  verticalLine: {
    color: '#009740',
    marginHorizontal: 6,
  },
  teamCell: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    // width: width / 2.8,
  },
  teamCellImg: {
    width: 20,
    height: 20,
    marginRight: 12,
  },
  cellText: {
    fontFamily: 'Montserrat400',
    fontSize: 16,
    lineHeight: 20,
    color: '#000000',
    opacity: 0.87,
  },
  mediumText: {
    fontFamily: 'Montserrat500',
    fontSize: 16,
    lineHeight: 20,
  },
  lightText: {
    fontFamily: 'Montserrat300',
  },
  blueText: {
    color: '#0C0054',
    opacity: 0.87,
  },
});
