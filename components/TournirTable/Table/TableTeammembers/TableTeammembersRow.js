import React from 'react';
import {
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  Dimensions,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
} from 'react-native';

const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);

const TableTeammembersRow = (props) => {
  const {
    index,
    player,
  } = props;

  return (
    <View style={styles.row}>
      <View style={[styles.cellCenter, { flex: 1 }]}>
        <Text style={styles.cellText}>{ index }</Text>
      </View>

      <View style={[styles.cellLeft, { flex: 4 }]}>
        <Text style={styles.cellText}>{ player.name }</Text>
      </View>

      <View style={[styles.cellRight, { flex: 3, paddingRight: 16 }]}>
        <Text style={styles.cellText}>{ player.birthday }</Text>
      </View>
    </View>
  )
};

export default TableTeammembersRow;

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 5,
    height: 36,
  },
  text: {
    color: '#000',
  },
  cellCenter: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cellLeft: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  cellRight: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  cellSmall: {
    width: width / 8,
    flex: 1,
  },
  verticalLine: {
    color: '#009740',
    marginHorizontal: 6,
  },
  teamCell: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    // width: width / 2.8,
  },
  teamCellImg: {
    width: 20,
    height: 20,
    marginRight: 12,
  },
  textLeft: {
    textAlign: 'left',
  },
  textRight: {
    textAlign: 'right',
  },
  cellText: {
    fontFamily: 'Montserrat400',
    fontSize: 16,
    lineHeight: 20,
    color: '#000000',
    opacity: 0.87,
  },
  mediumText: {
    fontFamily: 'Montserrat500',
    fontSize: 16,
    lineHeight: 20,
  },
  lightText: {
    fontFamily: 'Montserrat300',
  },
  blueText: {
    color: '#0C0054',
    opacity: 0.87,
  },
});
