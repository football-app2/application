import React from 'react';
import {
  Image,
  ImageBackground,
  KeyboardAvoidingView,
  Dimensions,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
} from 'react-native';

const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);

const TableTeammembersHead = () => {
  return (
    <View style={styles.head}>
      <View style={[styles.cellCenter, styles.cellSmall, { flex: 1 }]}>
        <Text style={[styles.teamCellText, styles.lightText]}>#</Text>
      </View>

      <View style={[styles.cellLeft, styles.cellSmall, { flex: 4 }]}>
        <Text style={[styles.teamCellText, styles.lightText, styles.headLeft]}>Cписок футболистов</Text>
      </View>

      <View style={[styles.cellRight, styles.cellSmall, { flex: 3, paddingRight: 16 }]}>
        <Text style={[styles.teamCellText, styles.lightText, styles.headRight]}>Год рождения</Text>
      </View>
    </View>
  )
};

export default TableTeammembersHead;

const styles = StyleSheet.create({
  head: {
    backgroundColor: '#F6F6F6',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 36,
  },
  headText: {
    color: '#000000',
    opacity: 0.87,
    textAlign: 'center',
  },
  headLeft: {
    textAlign: 'left',
  },
  headRight: {
    textAlign: 'right',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 5,
    height: 36,
  },
  text: {
    color: '#000',
  },
  cellCenter: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cellLeft: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  cellRight: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  cellSmall: {
    // width: width / 8,
    // flex: 1,
  },
  teamCell: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    // width: width / 2.8,
  },
  teamCellImg: {
    width: 20,
    height: 20,
    marginRight: 12,
  },
  teamCellText: {
    fontFamily: 'Montserrat400',
    fontSize: 16,
    lineHeight: 20,
    color: '#000000',
    opacity: 0.87,
  },
  mediumText: {
    fontFamily: 'Montserrat500',
    fontSize: 16,
    lineHeight: 20,
  },
  lightText: {
    fontFamily: 'Montserrat300',
  },
});
