import React, { useState, useEffect } from 'react';
import {
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  Image,
} from 'react-native';
import { Icon } from 'react-native-elements';

const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);

export const TournirType = ({ selectedBtn, setSelectedBtn, champType, onPressType }) => {
  return (
    <TouchableOpacity style={styles.container} activeOpacity={0.7} onPress={() => onPressType()}>
      <View style={styles.tournirType}>
        <Text style={styles.tournirTypeTxt}>{ champType === 1 ? 'Чемпионат города' : 'Чемпионат страны' }</Text>
      </View>
    </TouchableOpacity>
  )
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 72,
    paddingLeft: 24,
    borderTopWidth: 1,
    borderTopColor: '#EFF1F0',
    borderBottomWidth: 1,
    borderBottomColor: '#EFF1F0',
  },
  tournirType: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  tournirTypeTxt: {
    fontFamily: 'Montserrat600',
    fontSize: 16,
    lineHeight: 20,
    color: '#2B2A30',
    marginRight: 3,
  },
});
