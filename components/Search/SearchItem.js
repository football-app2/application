import React, { useState } from 'react';
import Constants from 'expo-constants';
import { ListItem } from 'react-native-elements';
import {
  Image,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
} from 'react-native';

import { IMGDOMAIN } from '../../api/utils/config';

export const SearchItem = ({ navigation, team, handlePressItem }) => {

    const { avatar, city_teams, name_teams } = team;

    return (
        <TouchableOpacity onPress={() =>
            handlePressItem !== undefined ?
                handlePressItem(team) :
                navigation.navigate('Team', { team: team })}
                activeOpacity={0.7}>
            <ListItem
                leftAvatar={{ rounded: true, source: { uri: `${IMGDOMAIN}/${avatar}` }, size: 34 }}
                title={name_teams}
                titleStyle={{
                  color: '#2B2A30',
                  fontFamily: 'Montserrat600',
                  fontSize: 16,
                  lineHeight: 20,
                  letterSpacing: -0.02,
                  paddingBottom: 2,
                }}
                subtitle={city_teams}
                subtitleStyle={{
                  color: '#000',
                  opacity: 0.54,
                  fontFamily: 'Montserrat600',
                  fontSize: 12,
                  lineHeight: 15,
                  letterSpacing: -0.02,
                }}
                containerStyle={{
                  backgroundColor: '#fff',
                  height: 60,
                }}
            />
        </TouchableOpacity>
    )
};
