import React, { useState, useEffect } from 'react';
import {
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
} from 'react-native';
import { Icon } from 'react-native-elements';

const width = Math.round(Dimensions.get('window').width);
const height = Math.round(Dimensions.get('window').height);

export const ToggleButtons = ({ titles, selectedBtn, setSelectedBtn, position, type }) => {
  return (
    <View style={[styles.buttonsContainer, position !== undefined && position === 'left' && styles.containerLeft]}>
      { type !== undefined && type === 'filter' && (
        <Icon
          name='filter-list'
          type='material'
          color='#0C0054'
        />
      )}

      <View style={{ marginLeft: type !== undefined && type === 'filter' ? 19 : 0, flexDirection: 'row' }}>
        {
          titles.map((item, key) => (
            <TouchableOpacity
              key={key}
              style={[styles.button, selectedBtn === key && styles.buttonActive, type !== undefined && type === 'filter' && styles.buttonFilter]}
              activeOpacity={0.8}
              onPress={() => setSelectedBtn(key)}
            >
              <Text style={[styles.buttonTxt, selectedBtn === key && styles.buttonTxtActive]}>{ item }</Text>
            </TouchableOpacity>
          ))
        }
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  buttonsContainer: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
  },
  containerLeft: {
    justifyContent: 'flex-start',
    paddingLeft: 19,
  },
  button: {
    width: width / 3 - 14,
    height: 28,
    borderRadius: 17,
    backgroundColor: 'rgba(12, 0, 84, 0.12)',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 5,
  },
  buttonFilter: {
    width: width / 3,
  },
  buttonTxt: {
    letterSpacing: -0.2,
    fontFamily: 'Montserrat500',
    fontSize: 15,
    lineHeight: 20,
    color: 'rgba(0, 0, 0, 0.87)',
  },
  buttonActive: {
    backgroundColor: '#0C0054',
  },
  buttonTxtActive: {
    color: '#fff',
  },
});
