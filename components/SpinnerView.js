import React from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';

export const SpinnerView = () => (
  <View style={styles.spinner}>
    <ActivityIndicator
      size="large"
      color="#242657"
    />
  </View>
);

const styles = StyleSheet.create({
  spinner: {
    position: 'absolute',
    left: '45%',
    top: 100,
    zIndex: 2,
    backgroundColor: '#fff',
    borderRadius: 18,
    width: 36,
    height: 36,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});