export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const GET_CURRENT_USER = 'GET_CURRENT_USER';
export const GET_BOOKMARKS = 'GET_BOOKMARKS';
export const SET_BOOKMARKS = 'SET_BOOKMARKS';
export const GET_LATESTQUERIES = 'GET_LATESTQUERIES';
export const SET_LATESTQUERIES = 'SET_LATESTQUERIES';

export const setCurrentUser = (user) => {
    return {
        type: SET_CURRENT_USER,
        payload: user
    }
};

export const getCurrentUser = () => {
    return {
        type: GET_CURRENT_USER
    }
};

export const getBookmarks = () => {
    return {
        type: GET_BOOKMARKS
    }
};

export const setBookmarks = (bookmarks) => {
    return {
        type: SET_BOOKMARKS,
        payload: bookmarks
    }
};

export const setLatestQueries = (user) => {
    return {
        type: SET_LATESTQUERIES,
        payload: user
    }
};

export const getLatestQueries = () => {
    return {
        type: GET_LATESTQUERIES
    }
};
