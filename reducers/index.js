import { createStore, combineReducers } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
// import storage from 'redux-persist/lib/storage';
import { AsyncStorage } from 'react-native';
import CurrentUserReducer from './CurrentUserReducer';

const rootReducer = combineReducers({
  currentUser: CurrentUserReducer,
});

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['navigation']
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
const store = createStore(persistedReducer);
let persistor = persistStore(store);

const configureStore = () => {
  return store;
};

export { configureStore, persistor };
