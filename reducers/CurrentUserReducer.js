import {
  SET_CURRENT_USER,
  GET_CURRENT_USER,
  GET_BOOKMARKS,
  SET_BOOKMARKS
} from '../actions';

const INITIAL_STATE = {
  currentUser: null,
  bookmarks: null
};

const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        ...state,
        currentUser: action.payload
      }
    case GET_BOOKMARKS:
      return state;
    case SET_BOOKMARKS:
      return {
        ...state,
        bookmarks: action.payload
      }
    case GET_CURRENT_USER:
      return state;
    default: return state
  }
};

export default userReducer;
