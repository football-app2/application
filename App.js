import React, { useState, useEffect } from 'react';
import { enableScreens } from 'react-native-screens';
enableScreens();
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import { AsyncStorage, Platform, StatusBar, StyleSheet, View } from 'react-native';
import * as Font from 'expo-font';
import { SpinnerView, ToastView } from './components';
import { configureStore, persistor } from './reducers';
import { setCurrentUser, getCurrentUser } from './actions';
import { UserAPI } from './api/user';

import AppNavigator from './navigation/AppNavigator';

const store = configureStore();

function App(props) {
  const [isLoadingComplete, setLoadingComplete] = useState(false);
  const [_authLoad, _setAuthLoad] = useState(false);
  const [_auth, _setAuth] = useState(false);

  const auth = () => {
    const values = {
      phone: '89101490462',
      password: '123456'
    };

    UserAPI.login(values)
      .then(response => {
        console.log('LOGIN - ',response);
        if(response.access_token !== undefined && parseInt(response.status) === 1) {
          AsyncStorage.setItem('user', JSON.stringify(response)).then(res => {
            store.dispatch(setCurrentUser(response));
            // navigation.navigate('Table');
            ToastView('Добро пожаловать!');
            // _setLoading(false);
            _setAuthLoad(true);
            _setAuth(true);
          });
        } else {
          // ToastView('Пожалуйста, проверьте правильность введённых данных');
          // _setLoading(false);
          _setAuthLoad(true);
          _setAuth(false);
        }
      });
  }

  const getUser = async (isCancelled) => {
    const data = await AsyncStorage.getItem('user');
    console.log('>>>>> USER DATA', data);
    if(data !== null) {
      console.log('OKKKAY');
      store.dispatch(setCurrentUser(JSON.parse(data)));
      _setAuthLoad(true);
      _setAuth(true);

      isCancelled = true;
    } else {
      console.log('--NOT AUTH');

      auth();

      // _setAuthLoad(true);
      // _setAuth(false);
    }
  }

  useEffect(() => {
    let isCancelled = false;

    if(!isCancelled) {
      getUser(isCancelled);
    }

    return () => {
      isCancelled = true;
    }
  }, []);

  console.log('_auth - ', _auth);

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={() => loadResourcesAsync()}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  } else {
    // AsyncStorage.setItem('user', {});

    // console.log('store.getState().currentUser', store.getState().currentUser);

    return (
      <Provider store={store}>
        <View style={styles.container}>
          {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
          {/*<AppNavigator />*/}
          <AppNavigator auth={_auth} load={_authLoad} />
        </View>
      </Provider>
    );
  }
}

async function loadResourcesAsync() {
  await Promise.all([
    Asset.loadAsync([
      require('./assets/splash.png'),
    ]),
    Font.loadAsync({
      'Montserrat300': require('./assets/fonts/Montserrat-Light.ttf'),
      'Montserrat400': require('./assets/fonts/Montserrat-Regular.ttf'),
      'Montserrat500': require('./assets/fonts/Montserrat-Medium.ttf'),
      'Montserrat600': require('./assets/fonts/Montserrat-SemiBold.ttf'),
      'Montserrat700': require('./assets/fonts/Montserrat-Bold.ttf'),
      'SFProDisplay700': require('./assets/fonts/SFProDisplay-Bold.ttf'),
    }),
  ]);
}

function handleLoadingError(error) {
  // In this case, you might want to report the error to your error reporting
  // service, for example Sentry
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
