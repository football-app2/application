import React, { useEffect } from 'react'
import {
    View,
    Text,
    StyleSheet,
    Image,
    Button,
    ScrollView,
    Alert, ImageBackground, Dimensions, StatusBar, TouchableOpacity } from 'react-native'
import { Feather } from '@expo/vector-icons'

import {HeaderButtons, Item} from "react-navigation-header-buttons";
import {AppHeaderIcon} from "../components/HeaderButton";
import {Card} from "react-native-elements";

import StickyParallaxHeader from 'react-native-sticky-parallax-header'



const text = 'Как писать статью: пошаговое руководство\n' +
    'Собравшись с мыслями и создав файл в Word, Google Docs или другом текстовом редакторе, автор зачастую впадает в ступор: он не знает, о чем писать, с чего начать, какой должна быть структура и подача материала. Причина этого — незнание алгоритма создания текстов. Поможет в решении проблемы пошаговое руководство по тому, как писать статью для публикации.\n' +
    'Шаг 1: выбор темы материала и ее анализ\n' +
    'Первый этап — поиск идеи для будущей статьи. Здесь возможны два варианта: подготовить текст на основе собственных интересов или исходя из информационных запросов потенциальных читателей.\n' +
    'Если оригинальная идея никак не приходит в голову, вдохновиться можно, просматривая телепередачи, посты в соцсетях, статьи различных сайтов и блогов.\n' +
    'Работа над текстом начинается задолго до того, как вы сядете за компьютер. В первую очередь необходимо понять, для чего пишется материал, к кому он будет обращен, какого результата и каким образом можно с его помощью добиться. После того как ответы на эти вопросы будут получены, работа пойдет проще: определена целевая аудитория, а значит, и стилистика написания.\n' +
    'Если вы пишете статью на заказ, обговорите с заказчиком цели и целеполагающие вопросы. Даже получив подробное техническое задание, автор должен задаться важным вопросом: а какова цель текста? Заказчик хочет с его помощью что-то прорекламировать или просто рассказать о своем продукте/услуге?\n' +
    'Умение ставить правильные целеполагающие вопросы перед написанием статьи для публикации важно еще и потому, что нередко заказчик затрудняется самостоятельно сформулировать цель текста. Кроме того, правильные вопросы помогают определить структуру будущей статьи.\n' +
    'Шаг 2: сбор информации\n' +
    'Главное — использовать достоверные источники. Это могут быть официальные документы или тематические интернет-ресурсы. Брать нужно только такую информацию, которая преподнесена объективно, безоценочно, желательно с подтвержденными фактами.\n' +
    'Материал для статьи можно найти:\n' +
    'в онлайн-изданиях;\n' +
    'на интернет-сервисах, аккумулирующих исследования по конкретной теме, таких как HubSpot;\n' +
    'в тематических блогах.\n' +
    'Например, перед вами стоит задача написать подробную статью о финансовой корпорации. Сведения о ней можно взять из отчетов по работе, опубликованных на официальном интернет-ресурсе, обязательным станет посещение сайта фондовой биржи, специализированных интернет-журналов.\n' +
    'Шаг 3: создаем заголовок\n' +
    'Написание заголовка — важнейший этап, поскольку именно название статьи будет видно интернет-пользователям в выдаче поисковиков. Интересный заголовок провоцирует желание кликнуть по нему. Иначе читатели не станут переходить по ссылке, отдав предпочтение материалу с более цепляющим названием.\n' +
    'Чтобы составить хороший заголовок, достаточно придерживаться двух правил:\n' +
    'в название статьи должна входить ключевая фраза, по которой текст оптимизируется;\n' +
    'заголовок должен быть интригующим, вызывающим желание узнать, о чем идет речь в материале.\n' +
    'Интерес у интернет-пользователей вызывают заголовки, содержащие вопрос.\n' +
    'Например, «Почему кризис 3 лет — кошмар для любой мамы?».\n' +
    'Неудачная альтернатива такому названию — «Особенности кризиса 3 лет у детей». Такой заголовок не интригует, не обозначает проблему, обещает скучное, сухое рассуждение на заезженную тему.\n'

export const ItemPostScreen = ({ navigation }) => {


    const { image, createdAt, content, title } = navigation.getParam('post');

    return (
        <View style={{backgroundColor: 'white', flex: 1}}>
            <StatusBar hidden={true} />
            <ImageBackground
                style={styles.wrapHeader}
                source={{uri: image}}
                borderBottomLeftRadius={40}
                borderBottomRightRadius={40}>
                <TouchableOpacity activeOpacity={0.7} style={styles.back} onPress={() => navigation.goBack()}>
                    <Feather name='arrow-left' size={25} color='white'/>
                </TouchableOpacity>
                <View style={styles.dateWrap}>
                    <Text style={styles.date}>{ createdAt }</Text>
                </View>
            </ImageBackground>
            <ScrollView style={styles.content}>
                <View>
                    <Text style={{
                        paddingTop: 20,
                        paddingLeft: 30,
                        paddingBottom: 20,
                        fontSize: 23,
                        fontWeight: 'bold',
                    }}>
                        { title }
                    </Text>
                </View>
                <View>
                    <Text style={{
                        paddingHorizontal: 14,
                        fontSize: 14,
                        fontWeight: 'normal',
                        opacity: 0.6,
                        justifyContent: 'flex-start',
                        textAlign: 'justify',
                        lineHeight: 26
                    }}>
                        { text }
                    </Text>
                </View>
            </ScrollView>
        </View>
    )
};

ItemPostScreen.navigationOptions = () => {
    return {
        headerShown: false,
    }
};

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 200
    },
    wrapHeader: {
        height: 300,
        width: '100%',
    },
    textWrap: {
        margin: 5,
        padding: 20,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'gray'
    },
    dateWrap: {
        position: 'absolute',
        top: 286,
        right: 50,
        borderRadius: 20,
        backgroundColor: '#009740'
    },
    date: {
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold',
        paddingHorizontal: 14,
        paddingVertical: 4
    },
    content: {
        marginTop: 20,
    },
    back: {
        position: 'absolute',
        top: 40,
        left: 20,
        backgroundColor: '#009740',
        padding: 5,
        borderRadius: 40
    }
});
