import React, { useState, useEffect } from 'react';
import {
    Image,
    ImageBackground,
    KeyboardAvoidingView,
    ActivityIndicator,
    Dimensions,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    Text,
    View,
    Button
} from 'react-native';
import {HorizontalDates, Table, TournirType} from '../../components';
import { TeamAPI } from '../../api/team';
import { MatchAPI } from '../../api/match';
import { CategoriesAPI } from '../../api/categories';
import {HeaderButtons, Item} from "react-navigation-header-buttons";
import { AppHeaderIcon} from "../components/HeaderButton";
import {TeamScreen} from "./TeamScreen";

const tableHead = ['#', 'Команда', 'З', 'П', 'О'];

export const TableScreen = ({ navigation }) => {
    const [_selectedBtn, _setSelectedBtn] = useState(0);
    const [_selectedCatID, _setSelectedCatID] = useState(0);
    const [_categories, _setCategories] = useState([]);

    const [_tableData, _setTableData] = useState([]);
    const [_loadTableData, _setLoadTableData] = useState(false);

    const [_tableDataRounds, _setTableDataRounds] = useState([]);
    const [_loadTableDataRounds, _setLoadTableDataRounds] = useState(false);

    const [_tableDataCalendar, _setTableDataCalendar] = useState([]);
    const [_loadTableDataCalendar, _setLoadTableDataCalendar] = useState(false);

    const [_champType, _setChampType] = useState(1);

    const [_page, _setPage] = useState(1);

    const onPressType = () => {
        _setPage(1);
        if(_champType === 1) {
            _setChampType(2)
        } else {
            _setChampType(1)
        }
    }

    const getTeamsTable = (type) => {
        TeamAPI.getTeamsTable({ to: 2100, from: 1900, champType: _champType, page: _page })
            .then(res => {
                //console.log('TABLE DATA - ', res);
                if(res && res.status !== 'error') {
                    if(type === 'more'){
                        _setTableData([..._tableData, ...res]);
                    } else {
                        _setTableData(res);
                    }
                    _setLoadTableData(true);
                }
            })
            .catch(err => console.error(err))
    }

    const getTeamsTableByCat = (id, type) => {
        TeamAPI.getTeamsTableByCat(id, { champType: _champType, page:  _page})
            .then(res => {
                //console.log('TABLE DATA - ', res);

                if(res && res.status !== 'error') {
                    if(type === 'more'){
                        _setTableData([..._tableData, ...res]);
                    } else {
                        _setTableData(res);
                    }
                    _setLoadTableData(true);
                }
            })
            .catch(err => console.error(err))
    }


    const getRounds = (type) => {
        MatchAPI.getRounds({ to: 2100, from: 1900, champType: _champType, page: _page })
            .then(res => {
                console.log('ROUNDS DATA - ', res);
                if(res && res.status !== 'error') {
                    if(type === 'more'){
                        _setTableDataRounds([..._tableDataRounds, ...res]);
                    } else {
                        _setTableDataRounds(res);
                    }
                    _setLoadTableDataRounds(true);
                }
            })
            .catch(err => console.error(err))
    }

    const getRoundsByCat = (id, type) => {
        MatchAPI.getRoundsByCat(id, { champType: _champType, page: _page })
            .then(res => {
                console.log('ROUNDS DATA - ', res);
                if(res && res.status !== 'error') {
                    if(type === 'more'){
                        _setTableDataRounds([..._tableDataRounds, ...res]);
                    } else {
                        _setTableDataRounds(res);
                    }
                    _setLoadTableDataRounds(true);
                }
            })
            .catch(err => console.error(err))
    }


    const getTours = (type) => {
        MatchAPI.getTours({ to: 2100, from: 1900, champType: _champType, page: _page })
            .then(res => {
                console.log('TOURS DATA - ', res);
                if(res && res.status !== 'error') {
                    if(type === 'more'){
                        _setTableDataCalendar([..._tableDataCalendar, ...res]);
                    } else {
                        _setTableDataCalendar(res);
                    }
                    _setLoadTableDataCalendar(true);
                }
            })
            .catch(err => console.error(err))
    }


    const getCategories = () => {
        CategoriesAPI.getCategories()
            .then(res => {
                if(res && res.status !== 'error') {
                    _setCategories(res);
                }
            })
            .catch(err => console.error(err))
    }

    const togglePressCategory = (id) => {
        _setPage(1);

        if(_selectedCatID === id) {
            _setSelectedCatID(0);
            onTabPress();
        } else {
            _setSelectedCatID(id);

            if(_selectedBtn === 0) {
                getTeamsTableByCat(id);
            } else if (_selectedBtn === 1) {
                getRoundsByCat(id);
            } else if (_selectedBtn === 2) {
                getToursByCat(id);
            }

        }
    }


    useEffect(() => {
        getCategories();
    }, [navigation])

    const onTabPress = (type) => {
        if(_selectedBtn === 0) {
            if(_selectedCatID !== 0) {
                getTeamsTableByCat(_selectedCatID, type);
            } else {
                getTeamsTable(type);
            }
        } else if (_selectedBtn === 1) {
            if(_selectedCatID !== 0) {
                getRoundsByCat(_selectedCatID, type);
            } else {
                getRounds(type);
            }
        } else if (_selectedBtn === 2) {
            if(_selectedCatID !== 0) {
                getToursByCat(_selectedCatID, type);
            } else {
                getTours(type);
            }
        }
    }

    const checkCount = () => {
        if(_selectedBtn === 0 && _tableData.length/_page === 7){
            return true;
        } else if (_selectedBtn === 1 && _tableDataRounds.length/_page === 20){
            return true;
        } else if (_selectedBtn === 2 && _tableDataCalendar.length/_page === 20){
            return true;
        } else {
            return false;
        }
    }

    useEffect(() => {
        onTabPress();
        //if(checkCount){
        _setPage(1);
        //}
    }, [navigation, _selectedBtn, _champType, _selectedCatID]);



    const goToTeam = ({ name }) => {
        navigation.navigate('ResultSearch', { name: name });
    };



    const handleMore = () => {
        if(checkCount() === true){
            _setPage(_page + 1);
            onTabPress('more');
        }
    }

    const renderTable = () => {
        if(_tableData.length !== 0) {
            return (
                <Table handleMore={handleMore} tableHead={tableHead} tableData={_tableData} type={'commands'} goToTeam={goToTeam} />
            )
        } else {
            return (
                <View>
                    <Text
                        style={{
                            marginVertical: 20,
                            marginHorizontal: 15,
                            textAlign: 'center',
                        }}
                    >
                        Данные не найдены
                    </Text>
                </View>
            )
        }
    };

    return (
        <View nestedScrollEnabled={true}  style={styles.container}>
            <TournirType champType={_champType} onPressType={onPressType} />
            <HorizontalDates
                togglePressCategory={togglePressCategory}
                categories={_categories}
                selectedCatID={_selectedCatID}
                setSelectedCatID={_setSelectedCatID}
            />
            <View style={{flex: 1, paddingBottom: 30}}>
                { renderTable() }
            </View>
        </View>
    )
};

TableScreen.navigationOptions = ({ navigation }) => ({
    headerTitle: 'Таблица',
    headerLeft: () => (
        <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
            <Item
                title='Toggle Drawer'
                iconName='ios-menu'
                onPress={() => navigation.toggleDrawer()}
            />
        </HeaderButtons>
    ),
    headerRight: () => (
        <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
            <Item
                title='Search'
                iconName='ios-search'
                onPress={() => navigation.navigate('Search')}
            />
        </HeaderButtons>
    ),
});

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1
    },
});
