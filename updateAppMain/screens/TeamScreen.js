import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Constants from 'expo-constants';
import {
    Image,
    ImageBackground,
    KeyboardAvoidingView,
    ActivityIndicator,
    AsyncStorage,
    Dimensions,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    Text,
    View,
} from 'react-native';
import {
    Table,
    ToastView,
} from '../../components';
import { ListItem } from 'react-native-elements';
import { TeamAPI } from '../../api/team';
import { IMGDOMAIN } from '../../api/utils/config';
import { setCurrentUser } from '../../actions';

const trainer = { id: 1, img: '', name: 'Иванов Иван Иванович' };
const tableData = [
    { id: 1, name: 'Иван Иванов', birthday: '2012' },
    { id: 2, name: 'Иван Иванов', birthday: '2012' },
];

const data = {
    trainer: {
        id: 1,
        avatar: '',
        name: 'Иванов Иван Иванович',
    },
    players: [
        { id: 1, name: 'Иван Иванов', birthday: '1988' },
        { id: 2, name: 'Иван Андреев', birthday: '1995' },
    ]
};

const TeamScreen = ({ navigation }) => {
    const [_data, _setData] = useState([]);
    const [_trainerAvatar, _setTrainerAvatar] = useState('');
    const team = navigation.getParam('team');

    const getTeammembersByTeamId = () => {
        if(team) {
            TeamAPI.getTeammembersByTeamId(team.id)
                .then(({ data }) => {
                    console.log('TEAMMEMBERS DATA - ', data);
                    if(data) {
                        const res = {
                            ...data,
                            trainer: {
                                name: data.trainer ? data.trainer.name : '',
                                avatar: data.trainer ? data.trainer.avatar : '',
                                id: data.trainer ? data.trainer.id : ''
                            }
                        }
                        _setData(res);
                        _setTrainerAvatar(`${IMGDOMAIN}${res.trainer.avatar}`);
                    }
                })
                .catch(err => console.error(err))
        }
    }

    useEffect(() => {
        getTeammembersByTeamId();
    },[navigation.getParam]);

    return (
        <ScrollView style={{ backgroundColor: '#fff' }}>
            { _data.length !== 0 && _trainerAvatar.length !== 0
                ?
                <>
                    <ListItem
                        leftAvatar={{ rounded: true, source: { uri: _trainerAvatar } }}
                        title={_data.trainer.name}
                        titleStyle={{
                            color: '#2B2A30',
                            fontFamily: 'Montserrat600',
                            fontSize: 16,
                            lineHeight: 20,
                            letterSpacing: -0.02,
                        }}
                        subtitle='Тренер'
                        subtitleStyle={{
                            color: '#000',
                            opacity: 0.7,
                            fontFamily: 'Montserrat600',
                            fontSize: 12,
                            lineHeight: 15,
                            letterSpacing: -0.02,
                        }}
                        containerStyle={{
                            backgroundColor: '#fff',
                            height: 88,
                        }}
                    />
                    <Table tableData={_data} type='teammembers' />
                </>
                :
                <Text>Привет</Text>
            }
        </ScrollView>
    )
};

const mapStateToProps = state => {
    return {
        currentUser: state.currentUser.currentUser
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setCurrentUser: (user) => {
            dispatch(setCurrentUser(user))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TeamScreen);
