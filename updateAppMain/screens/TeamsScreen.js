import React, {useState, useEffect} from "react";
import {View, Text, StyleSheet, ActivityIndicator, SafeAreaView} from 'react-native';
import {HeaderButtons, Item} from "react-navigation-header-buttons";
import {AppHeaderIcon} from "../components/HeaderButton";
import { TeamItems } from '../../components';
import { TeamAPI } from '../../api/team';


export const TeamsScreen = (props) => {
    const { navigation, scene, previous } = props;
    const [_loaded, _setLoaded] = useState(false);
    const [_teams, _setTeams] = useState([]);
    const [_page, _setPage] = useState(1);
    const [_status, _setStatus] = useState(true);
    // console.log('NAV props - ', navigation.state);
    //console.log('SCENE - ', scene);
    //console.log('PREVIOUS - ', previous);

    // useEffect(() => {
    //   BackHandler.addEventListener("hardwareBackPress", function() {console.log('BACKHANDLER'); return true;});
    //
    //   return () => {
    //     BackHandler.removeEventListener("hardwareBackPress", function() {return false;});
    //   }
    // }, []);

    // useBackHandler(() => {
    //   return true;
    // })

    const updateStatus = (status) =>{
        _setStatus(status);
    }

    const handleMore = () => {
        let page = _page + 1;
        getTeams(page);
    };

    const getTeams = (page) => {
        _setLoaded(true);
        TeamAPI.getTeamsMatches(page)
            .then(res => {
                //console.log('TEAMS SCREEN - ', res, _teams);
                let teams = _teams;
                if(res.teams !== undefined) {
                    for(let item in res.teams){
                        teams.push(res.teams[item])
                    }
                    _setTeams(teams);
                    _setLoaded(false);
                    _setPage(page);
                }
            })
            .catch(err => console.error(err))
    };

    useEffect(() => {
        TeamAPI.getTeamsMatches(1)
            .then(res => {
                //console.log('TEAMS SCREEN - ', res, _teams);
                if(res.teams !== undefined) {
                    _setTeams(res.teams);
                }
            })
            .catch(err => console.error(err));
    }, [navigation])


    return (
        <SafeAreaView style={{flex: 1}}>
            <View style={{ backgroundColor: '#fff', flex: 1}} >
                <View style={{ height: 16 }} />
                <View style={{flex: 1}}>
                    { _teams.length !== 0
                        ?
                        <TeamItems teams={_teams} status={_status}
                                   updateStatus={updateStatus} navigation={navigation}
                                   getTeams={getTeams}
                                   handleMore={handleMore}/>
                        :
                        <ActivityIndicator color='#009740' />
                    }
                    {_loaded && <ActivityIndicator color='#009740' />}
                </View>
            </View>
        </SafeAreaView>
    )
};


TeamsScreen.navigationOptions = ({ navigation }) => ({
    headerTitle: 'Матчи',
    headerLeft: () => (
        <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
            <Item
                title='Toggle Drawer'
                iconName='ios-menu'
                onPress={() => navigation.toggleDrawer()}
            />
        </HeaderButtons>
    )
});
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1
    },
    text: {
        color: 'black'
    }
});