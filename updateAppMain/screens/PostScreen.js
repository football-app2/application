import React, {useEffect, useState} from "react";
import {View, Text, StyleSheet, Button, FlatList, ActivityIndicator} from 'react-native';
import { HeaderButtons, Item } from 'react-navigation-header-buttons'
import { AppHeaderIcon } from '../components/HeaderButton'
import axios from "axios";
import { Post } from "../components/Post";

export const PostScreen = ({ navigation }) => {

    const [ dataPost, setDataPost ] = useState([]);
    const [ loading, setLoading ] = useState(true);
    const [ page, setPage ] = useState(0);
    const [ error, setError ] = useState(false);
    const [ refreshing, setRefreshing ] = useState(false);
    const [ message, setMessage ] = useState('');

    useEffect(() => {
        axios.get(`http://194.67.105.201:7999/getPostPagination?page=${page}`).then(({data}) => {
            setDataPost(dataPost.concat(data.data));
            setError(false);
            setLoading(false);
            setRefreshing(false);
        }).catch((e) => {
            setError(true);
            setRefreshing(false);
            setMessage(e.toString())
        })
    }, [refreshing, page, error]);

    const handleRefresh = () => {
        setRefreshing(true);
        setPage(0);
        setDataPost([]);
    };

    const handleLoadMore = () => {
        setPage(page + 1)
    };

    if(error){
        return (
            <View style={styles.container}>
                <Text>{message}</Text>
            </View>
        )
    }
    if(loading){
        return (
            <View style={[styles.container, styles.horizontal]}>
                <ActivityIndicator size="large" color="#009740" />
            </View>
        )
    }
    return (
        <View style={styles.wrapper}>
            <FlatList
                data={dataPost}
                keyExtractor={post => post.id.toString()}
                renderItem={({ item }) => <Post post={item} navigation={navigation} />}
                refreshing={refreshing}
                onRefresh={handleRefresh}
                onEndReached={handleLoadMore}
                onEndReachedThreshold={0}
            />
        </View>
    )
};

PostScreen.navigationOptions = ({ navigation }) => ({
    headerTitle: 'Новости',
    headerLeft: () =>
        <HeaderButtons HeaderButtonComponent={AppHeaderIcon} >
            <Item
                title='Toggle Drawer'
                iconName='ios-menu'
                onPress={() => navigation.toggleDrawer()}
            />
        </HeaderButtons>,
    headerRight: () => (
    <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
        <Item
            title='Search'
            iconName='ios-search'
            onPress={() => navigation.navigate('Search')}
        />
    </HeaderButtons>),
});

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    wrapper: {
        flex: 1,
        padding: 10,
    },
    error: {
        flex: 1,
        justifyContent: "center"
    },
    horizontal: {
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10
    }
});