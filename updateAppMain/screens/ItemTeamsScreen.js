import React from "react";
import {View, Text, StyleSheet, ScrollView, Image} from 'react-native';


export const ItemTeamsScreen = ({ navigation }) => {

    const { name_teams, name_categories, city_teams, avatar, createdAt  } = navigation.getParam('team');

    return(
        <View style={styles.container}>
            <Image source={{uri: avatar}} />
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#009740',
        flex: 0
    }
});