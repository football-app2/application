import React, { useState, useEffect } from 'react';
import { AsyncStorage, SafeAreaView, Dimensions, ScrollView, StyleSheet, Text, View } from 'react-native';
import { SearchBar, Icon } from 'react-native-elements';

import { SearchItem } from '../../components/Search/SearchItem';

import { SearchAPI } from '../../api/search';
import axios from "axios";

const width = Math.round(Dimensions.get('window').width);



export const SearchScreen = ({ navigation }) => {
    const [_search, _setSearch] = useState('');
    const [_searchQuery, _setSearchQuery] = useState('?');
    const [_results, _setResults] = useState([]);
    const [_latestQueries, _setLatestQueries] = useState([]);
    let timer = null;

    const handleChange = (value) => {
        _setSearch(value);
        _setSearchQuery(value);

        // timer = setTimeout(updateSearch, WAIT_INTERVAL);
    }

    const clearSearch = () => {
        console.log('Clear Search');
        _setSearch('');
        _setSearchQuery('');
    };

    useEffect(() => {
        //navigation.setOptions({handleChange: handleChange, search: _search, clearSearch: clearSearch});
        // SearchAPI.search({ s: _searchQuery })
        //     .then(res => {
        //         // console.log('SEARCH RES', res);
        //         _setResults(res);
        //     })

        axios.get(`http://194.67.105.201:7999/getCommandsSearch?page=0&search=${_searchQuery}`).then(({data}) => {
            _setResults(data.data)
        })
    }, [_searchQuery]);

    useEffect(() => {
        AsyncStorage.getItem('latestQueries').then(res => {
            // console.log("RES", res);
            if(res !== null) {
                // console.log('<<< ASYNC GET >>>', res);
                const data = JSON.parse(res);
                if(data.length > 5) {
                    const slicedData = data.concat().slice(0, -1);
                    _setLatestQueries(slicedData);
                    AsyncStorage.setItem('latestQueries', JSON.stringify(slicedData));
                } else {
                    _setLatestQueries(data);
                }
            }
        });
    }, [navigation]);

    useEffect(() => {
        // console.log('!!! _latestQueries - ',_latestQueries);
        if(_latestQueries.length !== 0) {
            AsyncStorage.setItem('latestQueries', JSON.stringify(_latestQueries));
        }
    }, [_latestQueries]);

    const handlePressItem = (team) => {
        navigation.navigate('Team', { team: team });
        _setLatestQueries(prevState => {
            let filteredState = prevState.filter(item => item.id !== team.id);
            console.log('FILTER', prevState.filter(item => item.id !== team.id));
            return [ team, ...filteredState ];
        });
    }

    return (
        <SafeAreaView>
            <View style={styles.container}>
                <View
                    style={{
                        alignItems: 'center',
                        flexDirection: 'row',
                        backgroundColor: '#009740'
                    }}
                >
                    <SearchBar
                        placeholder="Введите запрос"
                        onChangeText={handleChange}
                        value={_search}
                        containerStyle={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: width - 60,
                            backgroundColor: '#009740',
                            borderWidth: 0,
                            borderTopWidth: 0,
                            borderBottomWidth: 0,
                            marginLeft: 10,
                        }}
                        inputContainerStyle={{
                            borderRadius: 4,
                            backgroundColor: 'rgba(255, 255, 255, 0.7)',
                            paddingLeft: 9,
                        }}
                        inputStyle={{
                            fontFamily: 'Montserrat500',
                            fontSize: 15,
                            lineHeight: 20,
                            color: '#2B2A30',
                            opacity: 0.87,
                        }}
                        placeholderTextColor='#2B2A30'
                        searchIcon={false}
                        clearIcon={false}
                    />

                    {/*{ search !== '' && (*/}
                    <Icon
                        name='clear'
                        type='material'
                        color='#fff'
                        onPress={() => clearSearch()}
                    />
                    {/*)}*/}
                </View>
                {/** <View style={styles.headerContainer}>
                 <View style={styles.innerContainer}>
                 <View>
                 <TouchableOpacity activeOpacity={0.6} onPress={() => navigation.goBack()} style={[styles.backBtn, { marginBottom: 7 }]}>
                 <Icon
                 type='font-awesome'
                 name='angle-left'
                 color='#FFFFFF'
                 />
                 <Text style={styles.backText}>Back</Text>
                 </TouchableOpacity>
                 </View>

                 <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                 <SearchBar
                 placeholder="Введите запрос"
                 onChangeText={handleChange}
                 value={_search}
                 containerStyle={{
                justifyContent: 'center',
                alignItems: 'center',
                width: width - 50,
                backgroundColor: '#009740',
                borderWidth: 0,
                borderTopWidth: 0,
                borderBottomWidth: 0,
                marginLeft: 10,
              }}
                 inputContainerStyle={{
                borderRadius: 4,
                backgroundColor: 'rgba(255, 255, 255, 0.7)',
                paddingLeft: 9,
              }}
                 inputStyle={{
                fontFamily: 'Montserrat500',
                fontSize: 15,
                lineHeight: 20,
                color: '#2B2A30',
                opacity: 0.87,
              }}
                 placeholderTextColor='#2B2A30'
                 searchIcon={false}
                 clearIcon={false}
                 />

                 { _search !== '' && (
              <Icon
                name='clear'
                type='material'
                color='#fff'
                onPress={() => clearSearch()}
                />
            )}
                 </View>
                 </View>
                 </View> */}

                <ScrollView>
                    <View style={styles.head}>
                        <Text style={styles.headTxt}>Результаты поиска</Text>
                    </View>
                    <ScrollView style={{ minHeight: 60, backgroundColor: '#fff' }}>
                        { _results.length !== 0
                            ?
                            _results.map((team, key) => (
                                <SearchItem key={key} team={team} navigation={navigation} handlePressItem={handlePressItem} />
                            ))
                            :
                            <Text style={{ paddingTop: 10, paddingLeft: 17 }}>Не найдено</Text>
                        }
                    </ScrollView>

                    <View style={styles.head}>
                        <Text style={styles.headTxt}>Недавние запросы</Text>
                    </View>

                    <ScrollView>
                        { _latestQueries.map((team, key) => (
                            <SearchItem key={key} team={team} navigation={navigation} />
                        ))}
                    </ScrollView>
                </ScrollView>
            </View>
        </SafeAreaView>
    )
};

SearchScreen.navigationOptions = () => ({
    headerTitle: 'Поиск',
});

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
    },
    headerContainer: {
        backgroundColor: '#009740',
        justifyContent: 'flex-end',
        paddingVertical: 13,
        height: 136,
    },
    innerContainer: {
        flexDirection: 'column',
        alignItems: 'flex-start',
    },
    head: {
        backgroundColor: '#F6F6F6',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        paddingLeft: 16,
        height: 36,
    },
    headText: {
        fontSize: 16,
        lineHeight: 20,
        fontFamily: 'Montserrat300',
        color: '#000000',
        opacity: 0.87,
    },
    backBtn: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginLeft: 8.5,
        marginBottom: 6.5,
    },
    backText: {
        color: '#fff',
        fontSize: 17,
        lineHeight: 22,
        marginLeft: 5,
        paddingTop: 1,
    },
});

