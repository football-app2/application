import React, { useState, useEffect } from 'react';
import {
    StyleSheet, Text, View,} from 'react-native';
import {
    HorizontalDates,
    Table, TournirType,
} from '../../components';
import { TeamAPI } from '../../api/team';
import { MatchAPI } from '../../api/match';
import { CategoriesAPI } from '../../api/categories';
import {HeaderButtons, Item} from "react-navigation-header-buttons";
import {AppHeaderIcon} from "../components/HeaderButton";


export const ResultScreen = ({ navigation }) => {
    const [_selectedBtn, _setSelectedBtn] = useState(1);
    const [_selectedCatID, _setSelectedCatID] = useState(1);
    const [_categories, _setCategories] = useState([]);


    const [_tableDataRounds, _setTableDataRounds] = useState([]);
    const [_loadTableDataRounds, _setLoadTableDataRounds] = useState(false);

    const [_champType, _setChampType] = useState(1);

    const [_page, _setPage] = useState(1);

    const onPressType = () => {
        _setPage(1);
        if(_champType === 1) {
            _setChampType(2)
        } else {
            _setChampType(1)
        }
    }

    const getRounds = (type) => {
        MatchAPI.getRounds({ to: 2100, from: 1900, champType: _champType, page: _page })
            .then(res => {
                console.log('ROUNDS DATA - ', res);
                if(res && res.status !== 'error') {
                    if(type === 'more'){
                        _setTableDataRounds([..._tableDataRounds, ...res]);
                    } else {
                        _setTableDataRounds(res);
                    }
                    _setLoadTableDataRounds(true);
                }
            })
            .catch(err => console.error(err))
    }

    const getRoundsByCat = (id, type) => {
        MatchAPI.getRoundsByCat(id, { champType: _champType, page: _page })
            .then(res => {
                if(res && res.status !== 'error') {
                    if(type === 'more'){
                        _setTableDataRounds([..._tableDataRounds, ...res]);
                    } else {
                        _setTableDataRounds(res);
                    }
                    _setLoadTableDataRounds(true);
                }
            })
            .catch(err => console.error(err))
    };


    const getCategories = () => {
        CategoriesAPI.getCategories()
            .then(res => {
                if(res && res.status !== 'error') {
                    _setCategories(res);
                }
            })
            .catch(err => console.error(err))
    }

    const togglePressCategory = (id) => {
        _setPage(id);

        if(_selectedCatID === id) {
            _setSelectedCatID(0);
            onTabPress();
        } else {
            _setSelectedCatID(id);

            if(_selectedBtn === 0) {
            } else if (_selectedBtn === 2) {
                getRoundsByCat(id);
            }

        }
    }


    useEffect(() => {
        getCategories();
    }, [navigation]);

    const onTabPress = (type) => {
        if (_selectedBtn === 1) {
            if(_selectedCatID !== 0) {
                getRoundsByCat(_selectedCatID, type);
            } else {
                getRounds(type);
            }
        }
    };

    const checkCount = () => {
        if (_selectedBtn === 1 && _tableDataRounds.length / _page === 20) {
            return true;
        }
    }

    useEffect(() => {
        onTabPress();
        //if(checkCount){
        _setPage(1);
        //}
    }, [navigation, _selectedBtn, _champType, _selectedCatID]);



    const goToTeam = (team) => {
        navigation.navigate('Team', { team });
    }



    const handleMore = () => {
        if(checkCount() === true){
            _setPage(_page + 1);
            onTabPress('more');
        }
    }

    const renderTable = () => {
        if(_tableDataRounds.length !== 0) {
            return (
                <Table tableHead={_tableDataRounds} tableData={_tableDataRounds} type={'rounds'} goToTeam={goToTeam}  />
            )
        } else {
            return (
                <View>
                    <Text
                        style={{
                            marginVertical: 20,
                            marginHorizontal: 15,
                            textAlign: 'center',
                        }}
                    >
                        Данные не найдены
                    </Text>
                </View>
            )
        }
    }

    return (
        <View nestedScrollEnabled={true}  style={styles.container}>
            <TournirType champType={_champType} onPressType={onPressType} />
            <HorizontalDates
                togglePressCategory={togglePressCategory}
                categories={_categories}
                selectedCatID={_selectedCatID}
                setSelectedCatID={_setSelectedCatID}
            />
            <View style={{flex: 1, paddingBottom: 30}}>
                { renderTable() }
            </View>
        </View>
    )
};

ResultScreen.navigationOptions = ({ navigation }) => ({
    headerTitle: 'Результаты',
    headerLeft: () => (
        <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
            <Item
                title='Toggle Drawer'
                iconName='ios-menu'
                onPress={() => navigation.toggleDrawer()}
            />
        </HeaderButtons>
    ),
    headerRight: () => (
        <HeaderButtons HeaderButtonComponent={AppHeaderIcon}>
            <Item
                title='Search'
                iconName='ios-search'
                onPress={() => navigation.navigate('Search')}
            />
        </HeaderButtons>)
});

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1
    },
    text: {
        color: 'black'
    }
});