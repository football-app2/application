import React from "react";
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity, Image } from 'react-native'
import {IMGDOMAIN} from "../../api/utils/config";
import {ListItem} from "react-native-elements";

export const TeamItem = ({ team }) => {

    const { avatar, city_teams, name_teams } = team;

    return (
        <ListItem
            // chevron
            leftAvatar={{ rounded: true, source: { uri: `${IMGDOMAIN}/${avatar}` } }}
            title={name_teams}
            titleStyle={{
                color: '#009740',
                fontFamily: 'Montserrat600',
                fontSize: 16,
                lineHeight: 20,
                letterSpacing: -0.02,
            }}
            subtitle={city_teams}
            subtitleStyle={{
                color: '#000',
                opacity: 0.7,
                fontFamily: 'Montserrat600',
                fontSize: 12,
                lineHeight: 15,
                letterSpacing: -0.02,
            }}
            containerStyle={{
                backgroundColor: '#F6F6F6',
                height: 72,
            }}
            // onPress={() => navigation.navigate('Team', { team })}
        />
    )
}