import React, {useEffect, useState} from "react";
import {View, Text, Image, StyleSheet, Button} from "react-native";
import {DrawerNavigatorItems} from "react-navigation-drawer";
import axios from "axios";


export const Sidebar = props => {

    const [ uri, setUri ] = useState(null);

    useEffect(() => {
        axios.get('http://194.67.105.201:7999/getLogo').then(({data}) => {
            setUri(data.img)
        })
    });

    return(
        <View style={styles.container}>
            <View style={styles.container1}>
                <Image
                    source={uri ? {uri: uri } : null} style={styles.img}
                />
            </View>
            <View style={{flex: 3}}>
                <DrawerNavigatorItems {...props} labelStyle={styles.container2}/>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    container1: {
        flex: 1,
        height: 200,
        width: '100%',
    },
    img: {
        height: 84,
        width: '100%'
    },
    container2: {
        paddingLeft: 5,
        fontSize: 16
    }
});