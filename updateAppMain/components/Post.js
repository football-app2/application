import React from 'react'
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity, Image, ImageBackground, ScrollView
} from 'react-native'
import {Card} from "react-native-elements";

export const Post = ({ post, navigation }) => {

    const { image, title } = post;

    return (
        <ScrollView style={styles.post}>
            <TouchableOpacity activeOpacity={0.7} onPress={() => navigation.navigate('OnePost', { post: post })}>
                <ImageBackground source={{uri: image}} style={styles.image}>
                    <View style={styles.textWrap}>
                        <Text style={styles.title}>
                            { title }
                        </Text>
                    </View>
                </ImageBackground>
            </TouchableOpacity>
        </ScrollView>
    )
};


const styles = StyleSheet.create({
    post: {
        marginBottom: 25,
        overflow: 'hidden',
    },
    image: {
        width: '100%',
        height: 200
    },
    textWrap: {
        backgroundColor: 'rgba(0, 0, 0, 0.8)',
        paddingVertical: 7,
        alignItems: 'flex-start',
        width: '100%',
        paddingLeft: 20,
    },
    title: {
        color: '#fff',
        fontWeight: 'bold'
    }
});
