import React from 'react'
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createDrawerNavigator } from 'react-navigation-drawer'
import { PostScreen } from '../screens/PostScreen'
import { ResultScreen } from '../screens/ResultScreen'
import { TableScreen } from '../screens/TableScreen'
import { TeamsScreen } from '../screens/TeamsScreen'
import { SearchScreen } from "../screens/SearchScreen";
import {ItemTeamsScreen} from "../screens/ItemTeamsScreen";
import SettingsScreen from "../../screens/SettingsScreen";
import {ItemPostScreen} from "../screens/ItemPostScreen";
import {Sidebar} from "../components/Sidebar";
import TeamScreen  from "../screens/TeamScreen";
import {TimeScreen} from "../screens/TimeScreen";

const navigatorOptions = {
    defaultNavigationOptions: {
        headerStyle: {
            backgroundColor: '#009740'
        },
        headerTintColor: 'white',
    }
};

const createPost = createStackNavigator(
    {
        Post: PostScreen,
        OnePost: ItemPostScreen
    },
    navigatorOptions
);

const createTable = createStackNavigator(
    {
        Table: TableScreen,
        Search: SearchScreen,
        Team: TeamScreen,
    },
    navigatorOptions
);

const createResult = createStackNavigator(
    {
        Result: ResultScreen,
        Search: SearchScreen,
        Team: TeamScreen,
    },
    navigatorOptions
);

const createTime = createStackNavigator(
    {
        Time: TimeScreen,
        Search: SearchScreen,
        Team: TeamScreen,
    },
    navigatorOptions
);

const createSettings = createStackNavigator(
    {
        Time: SettingsScreen
    },
    navigatorOptions
);

const MainNavigator = createDrawerNavigator(
    {
        Post: {
            screen: createPost,
            navigationOptions: {
                drawerLabel: 'Новости',
            }
        },
        Table: {
            screen: createTable,
            navigationOptions: {
                drawerLabel: 'Таблица',
            }
        },
        Result: {
            screen: createResult,
            navigationOptions: {
                drawerLabel: 'Результаты',
            }
        },
        Time: {
            screen: createTime,
            navigationOptions: {
                drawerLabel: 'Калентрадь',
            }
        },
        createSettings: {
            screen: createSettings,
            navigationOptions: {
                drawerLabel: 'Настройки',
            }
        }
    },
    {
        contentOptions: {
            activeTintColor: '#009740',
        },
        contentComponent: props => <Sidebar {...props}/>,
        drawerWidth: 280,
        hideStatusBar: true
    }
);

export const UpdateAppNavigation = createAppContainer(MainNavigator);
